package main //声明所在包

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"go_test/day1"
	"net"
	"sync"

	//"github.com/jasonlvhit/gocron"
	"github.com/go-co-op/gocron"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"
) //导入fmt包

var name = "zhushuilong"

const (
	PokerLevel2 = iota + 2
	PokerLevel3
	PokerLevel4
)

func main() { //函数入口，以main为方法名
	var cstZone = time.FixedZone("CST", 8*3600) // 东八
	log.Println(time.Now().In(cstZone).Format("20060102150405"))
	log.Println(time.Now().Format("20060102150405"))
	//log.Errorf("Post writer.CreateFormFile() filePath:%s err:%v", "/ddsds/yyyy.jpg", "sdsdsddsd")
	//log.Errorf("ddsddsds")
	//var alarmNameMap = map[string][]day1.DeviceGroup{}
	//alarmNameMap["3333"] = append(alarmNameMap["3333"], day1.DeviceGroup{ID: 1, DeviceGroupId: "99999"})
	//alarmNameMap["3333"] = append(alarmNameMap["3333"], day1.DeviceGroup{ID: 2, DeviceGroupId: "9996767699"})
	//fmt.Println(alarmNameMap["3333"])
	//fmt.Println(alarmNameMap["3333"][0])
	//fmt.Println(alarmNameMap["3333"][1])
	//fmt.Println(time.Now().Format("2006-01-02 15:04:05"))
	//fmt.Println(time.Now().Format("2006-01-02 15:04:05")[:15] + "0:00")
	//fmt.Println(time.Now())
	//gg := "/d/gg/h/ii/ooooo.jpg"
	//var yy int
	//yy = 88
	//fmt.Println(uint(yy))
	//vv := make([]string, 0)
	//vv = append(vv, "")
	//vv = append(vv, "")
	//vv = append(vv, "ttyyty")
	//fmt.Println(vv)
	//yy, _ := strconv.ParseInt("1111111111111111111", 10, 64)
	//fmt.Println(yy)
	//	yy := fmt.Sprintf("%s deviceId:%s", time.Now().Format("2006/01/02"), "00000")
	//	fmt.Println(yy)
	//	if 1 == 2 {
	//		goto here
	//	}
	//	fmt.Println(yy)
	//here:
	//	fmt.Println("dsdsdddsdsds")
	//var ppp = "3"
	//var lll = "10"
	//ppp1, _ := strconv.ParseFloat(ppp, 32)
	//fmt.Println(time.Unix(1677480053, 0).Format("2006-01-02 15:04:05"))
	//lll1, _ := strconv.ParseFloat(lll, 32)
	//fmt.Println(fmt.Sprintf("%.2f", (lll1-ppp1)*100/lll1))
	//
	//var yyy = []string{"www", "ttt", "iii"}
	//fmt.Println(yyy[0:3])
	//var ChannelIdEnum = map[string]int64{}
	//if v, ok := ChannelIdEnum["888"]; ok {
	//	fmt.Printf("rr:%v,ii:%v\n", v, ok)
	//} else {
	//	fmt.Printf("rr:%v,ii:%v\n", v, ok)
	//}
	//fmt.Println(ChannelIdEnum["888"])
	//var tt int64
	//tt = time.Now().Unix()
	////tt = 2000000000
	//fmt.Println(tt == 2000000000)
	//fmt.Printf("deviceId:%s,callUrl:%s,code:%d\n", "trrttr", "tttt", 200)

	//var channel1, channel2 = make(chan int, 1), make(chan int, 1)
	//channel1 <- 1
	//channel2 <- 3
	//var rr, tt = add(1, 2), calc(3, 9)
	//fmt.Println(rr, tt)
	//fmt.Println(strings.Split("1.22.345", "."))
	//ChannelMessageBox        map[string]chan MediaStreamChannelInfo // channel
	//fmt.Println([]string{"11", "22", "00"})
	//var kV sync.Map
	//ppp := make(chan int)
	//kV.Store("123", ppp)
	//go inchannel(kV, "123")
	//select {
	//case code := <-ppp:
	//	fmt.Println("code:", code)
	//case <-time.After(6 * time.Second):
	//	fmt.Println("code:", 78965)
	//	kV.Delete("123")
	//	fmt.Println("dddd:", ppp)
	//}
	//
	//fmt.Println("opoppopop:", 456)
	//var uuu = day1.DeviceGroup{ID: 12, DeviceGroupId: "456789", DeviceGroupName: "reer", Level: "weweee"}
	//kV.Store("id", uuu)
	//fmt.Println(kV.Load("id"))
	//yio, ok := kV.Load("id")
	//fmt.Println(yio.(day1.DeviceGroup).DeviceGroupId, ok)
	//if v, ok := yio.(day1.DeviceGroup); ok {
	//	fmt.Println("wqeqweeqe", v, ok)
	//} else {
	//	fmt.Println("wqeqweeqe", v, ok)
	//	fmt.Println(v, ok)
	//}
	//
	////fmt.Println(yio.(int), ok)
	//kV.Store("id", "87677676")
	//fmt.Println(kV.Load("id"))
	//kV.Delete("id")
	//fmt.Println(kV.Load("id"))
	////获取时间戳
	//timestamp := time.Now().Unix()
	//fmt.Println(timestamp)
	////格式化为字符串,tm为Time类型
	//tm := time.Unix(timestamp, 0)
	//fmt.Println(tm)
	//fmt.Println(tm.Format("2006-01-02 03:04:05 PM"))
	//fmt.Println(tm.Format("02/01/2006 15:04:05 PM"))
	////从字符串转为时间戳，第一个参数是格式，第二个是要转换的时间字符串
	//tm2, _ := time.Parse("01/02/2006", "02/08/2015")
	//fmt.Println(tm2.Unix())
	//fmt.Println("2023/2/10 5:42:44"[0:9])
	//fmt.Println(strings.Index("2023/2/10 5:42:44", " "))
	//fmt.Println(time.Now().Format("2006/1/2"))
	//fmt.Println(time.Now().Format("2006/1/2") == "2023/2/10 5:42:44"[0:9])
	//fmt.Println(strings.Split("1.22.345", "."))
	//for {
	//	goCron()
	//}
	//var tt = 0
	//var rr = [...]int{0, 0, 1, 7, 9}
	//i := 0
	//for 0 == tt {
	//	tt = rr[i]
	//	i++
	//	fmt.Println(tt)
	//}
	//str := "11.jpg"
	//str2 := strings.Replace(str, ".", "-Thumbnails.", 1)
	//fmt.Println(path.Join("/fff/tt", "/ooo", "4ooo", "/dfdtrt.jpg"))
	//mapTest()
	//fmt.Println(8 % 2)
	//time.Sleep(5 * time.Second)
	//fmt.Println(8 % 2)
	//absPath := fmt.Sprintf("%v-%v%v", 56555555, 1, "uuu.jpg")
	//fmt.Println(absPath)
	//fmt.Println(time.Now().Format("2006-01-02"))
	//var myConcurrentMap = sync.Map{}
	//changeMap(&myConcurrentMap)
	////取出数据
	//name, ok := myConcurrentMap.Load(141)
	//fmt.Println(name.(string))
	//if !ok {
	//	fmt.Println("不存在")
	//	return
	//}
	//打印值  li_ming
	//fmt.Println(name)
	//allResultMap := make(sync.Map, 0)
	//fmt.Println(allResultMap)

	//fmt.Println(allResultMap)
	//if phl, ok := allResultMap[name]; 1 > phl {
	//	fmt.Println(phl, ok)
	//}
	//re, _ := regexp.Compile(`^1\d{9}$`)
	//matched1 := re.MatchString("1669999999")
	//fmt.Println(time.Unix(1669107108, 0).Format("2006-01-02 15:04:05"))
	//str := "111"
	//fmt.Println(str)
	//newStr := str
	//newStr = "1"
	//fmt.Println(str)
	//fmt.Println(newStr)
	//log.Errorln(time.Now().Unix())
	//var ff int
	//ff = 1
	//strconv.ParseInt(ff, 10, 64)
	//str := "P1"
	//fmt.Println(str[0:1])
	//fmt.Println(str[1:])
	//_, err := os.OpenFile("D:/temp/AiriaCloudLog/GuanDanLog.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModeAppend|os.ModePerm)
	//fmt.Println(err)
	//log.Println(false || true || false)
	//log.Warnln(false || true || false)
	//log.Errorln(time.Now().Unix())
	//log.Errorf("%v", 111)
	//str1 := new(string)
	//*str1 = "1"
	//str2 := str1
	//*str2 = "2"
	//if str1 == str2 {
	//	fmt.Println("333")
	//}
	//fmt.Println(*str1)
	//fmt.Println(*str2)
	//log.Errorf("websocket user_id[%d] 3%v", 1, 2)
	//tt := "222"
	//fmt.Println(tt[0:2])
	//if tt == "222" {
	//	fmt.Println("111")
	//}
	//fmt.Println(PokerLevel2)
	//fmt.Println(PokerLevel3)
	//fmt.Println(PokerLevel4)
	//defer executePanic()
	//pc := []int{1}
	//fmt.Println("eeeee", pc[2:])
	//sort.Sort(pc)
	// 2
	//a, b := 1, 2
	//fmt.Println(a, b)
	//a, b = b, a
	//fmt.Println(a, b)
	//if "1" == "1" {
	//	fmt.Println("Result 2:", 1)
	//}
	//dayTime := time.Now().Format("2006-01-02 15:04")
	//fmt.Println("Result 2:", dayTime)
	//fmt.Println("Result 2:", dayTime[0:15]+"0:00")
	//fmt.Println("Result 2:", dayTime[0:10])
	//tt := make(map[string]int)
	//for _, v := range tt {
	//	fmt.Println("Result 2:", v)
	//}
	//tt["H"] = 1
	//tt["S"] = 2
	//tt["D"] = 3
	//tt["C"] = 4
	//fmt.Println("Result 2:", tt)
	//str := "11122334.4"
	//res2 := strings.Index(str, ".")
	//fmt.Println("Result 2:", res2)
	//fmt.Println(str[0:strings.Index("11122334.4", ".")])
	//list := make([]string, 0)
	//list := new(string)
	//fmt.Println(*list)
	//var eee interface{}
	//eee = list
	//fmt.Println(eee)
	//yyy := eee.(string)
	//fmt.Println(yyy)
	//err := errors.New("image id 不能为空")
	//fmt.Println(err.Error())
	//yy := make([]int, 0)
	//yy = append(yy, 1)
	//yy = append(yy, 2)
	//rr := IntListToInString(yy)
	//fmt.Println(rr)
	//start := time.Now()
	//time.Sleep(1 * time.Second)
	//end := time.Now()
	//fmt.Println(time.Now().Add(-time.Minute * 10).String())
	//var t *string
	////var tt string
	//fmt.Println(reflect.ValueOf(t).Kind() == reflect.Ptr)
	//printOut()
	//de := deferTest()
	//var value interface{}
	//value = 1
	//fmt.Println(value.(int))
	//fmt.Println(de)
	//sameValiable(1)

	//var a, b int = 1, 2
	//c := add(a, b)
	//sum, avg := calc(a, b)
	//fmt.Println(a, b, c, sum, avg)
	//sysDelimiter()
	//mapTest()
	//interfacesToStrings()
	//rangeTest()
	//preTest()
	//timingTask()
	//InterfaceTest()
	//StringTest()
	//ChanTest()
	//go goCron()
	//fmt.Println("main start!")
	//time.Sleep(time.Minute * 30)
	//timeConser()
	//isNullTest()
	//Strconv()
	//Xlsx()
	//demo([]int{1, 2, 3, 4})
	//p1 := make([]day1.DeviceGroup, 0)
	//d1 := new(day1.DeviceGroup)
	//d1.ID = 1
	//d1.DeviceGroupName = "11"
	//log.Println("d1:", *d1)
	//p1 = append(p1, *d1)
	//d2 := new(day1.DeviceGroup)
	//d2.ID = 2
	//d2.DeviceGroupName = "22"
	//p1 = append(p1, *d2)
	//log.Println("p1:", p1)
	//d3 := new(day1.DeviceGroup)
	//d3.ID = 3
	//d3.DeviceGroupName = "33"
	//p1 = append(p1, *d3)
	//sets := product([]interface{}{d1, d2, d3}, []interface{}{1, 2, 3}, []interface{}{1, 2, 3})
	//fmt.Println(len(sets))
	//for _, set := range sets {
	//	fmt.Println(set)
	//}
	//IsExitMap()

}

func changeMap(data *sync.Map) {
	//存储数据
	data.Store(1, "li_ming")
}

func FindListIndex(data []interface{}, target interface{}) int {
	for index, value := range data {
		if value == target {
			return index
		}
	}
	return -1
}

//executePanic defer异常处理
func executePanic() {
	if errMSg := recover(); errMSg != nil {
		tt := fmt.Sprintf("%v", errMSg)
		fmt.Println(tt)
	}
	//defer fmt.Println("defer func")
	//panic("This is Panic Situation")
	//fmt.Println("The function executes Completely")
	//d := new(day1.DeviceGroup)
	//d.ID = 1
	//fmt.Println(*d)
}

//deferTest defer传值测试
func deferTest() (a int) {
	a = 1
	defer func() {
		a = 2
		fmt.Println("testDefer")
	}()
	fmt.Println("mainDefer")
	return a
}

//printOut 输出练习
func printOut() {

	fmt.Println("Hello,World!") //打印Hello，World!

	a := 1
	fmt.Printf("a=%d\n", a)
	fmt.Printf("a=%v\n", a)

	var i interface{} = 23
	fmt.Printf("%v\n", i)

	c := fmt.Sprintf("%6.2f", 12.0)
	fmt.Println(c)

	d := fmt.Sprintf("%d %d %#[1]x %#x", 16, 17)
	fmt.Println(d)

	// 两参数格式化
	title1 := fmt.Sprintf("已采集%d个药草, 还需要%d个完成任务", 1, 2)
	fmt.Println(title1)
	t1 := 1
	t2 := 2
	title2 := fmt.Sprintf("已采集%d个药草, 还需要%d个完成任务", t1, t2)
	fmt.Println(title2)

	fmt.Println(day1.Name)

	listG := make([]day1.DeviceGroup, 0)
	listG = nil
	//fmt.Println(listG[0].ID)
	fmt.Println(len(listG))
	fmt.Println(listG[0])
	de := new(day1.DeviceGroup)
	if *de == (day1.DeviceGroup{}) {
		fmt.Println(day1.Name)
	}
	de.ID = 1
	de.Level = "1111"
	de.DeviceGroupName = "33333"
	fmt.Println(*de)

}

//add 相加
func add(a, b int) (c int) {
	c = a + b
	return
}

//calc 算平均数
func calc(a, b int) (avg int) {
	//sum = a + b
	avg = (a + b) / 2

	return
}

//sameValiable 测试同一方法中变量名称是否可以同名
func sameValiable(a int) {
	fmt.Println("a=", a)
	a, b := 2, 3
	fmt.Println("a=", a)
	fmt.Println("b=", b)
	a, c := 4, 5
	fmt.Println("a=", a)
	fmt.Println("c=", c)
}

//sysDelimiter 跨系统获取路径名分隔符
func sysDelimiter() {
	sep := string(filepath.Separator)
	fmt.Println(sep)
	path := "D:" + sep + "AiRiA" + sep + "code" + sep + "AiriaCloud" + sep + "conf" + sep + "config.toml"
	fmt.Println(path)
}

//mapTest map练习测试
func mapTest() {
	testMap := make(map[string]string, 0)
	testMap["test"] = "1"
	testMap["111"] = "2"
	testMap["111"] = "3"
	for k, _ := range testMap {
		if "test" == k {
			delete(testMap, k)
		}

	}
	fmt.Println(testMap)
	fmt.Println(testMap["111"])
	temp := testMap["666"]
	fmt.Println(temp)
	fmt.Println(reflect.TypeOf(temp))
}

//getName 任意类型数组转换为string
func getName(params ...interface{}) {

	strArray := make([]string, len(params))
	for i, arg := range params {
		strArray[i] = arg.(string)
	}
	aa := strings.Join(strArray, "_")
	fmt.Println(aa)
}

func interfacesToStrings() {
	getName("redis", "100", "master")
}

//getName 循环次数在循环开始前就已经确定，循环内改变切片的长度，不影响循环次数。
func rangeTest() {
	v := make([]int, 3, 5)
	fmt.Printf("len=%d cap=%d slice=%v\n", len(v), cap(v), v)
	for i := range v {
		v = append(v, i)
	}
	fmt.Println("运行结束")
}

func preTest() {
	var a *int
	var t = 10
	fmt.Println("a=", a)
	a = &t
	fmt.Println("a=", a)
	fmt.Println("a=", *a)
	b := a
	*b = 2
	fmt.Println("a=", *a)
	fmt.Println("b=", *b)

}
func timingTask() {
	tiker := time.NewTicker(time.Second * 5)
	for {
		w := fmt.Sprintf("%v:主go程执行程序main OK", time.Now())
		fmt.Println(w)
		<-tiker.C
	}
}

func InterfaceTest() {
	tt := make([]interface{}, 0)
	tt = append(tt, "11111")
	tt = append(tt, 111)
	tt = append(tt, 5.369)
	tt = append(tt, "555511")

	pp := make([]interface{}, 0)
	pp = tt
	fmt.Println(pp)
}

func StringTest() {
	//开始时间
	startTime := time.Now()
	time.Sleep(1 * time.Microsecond)
	//结束时间
	endTime := time.Now()
	//执行时间
	latencyTime := endTime.Sub(startTime).Milliseconds()
	fmt.Println(latencyTime)
	tempTime, _ := strconv.ParseFloat(fmt.Sprintf("%.3f", float64(endTime.Sub(startTime).Microseconds())/float64(1000)), 64)
	fmt.Println(tempTime)
	fmt.Println(strings.ToLower("HeLLo world"))
	temp := "11"
	if 0 == len(temp) {
		fmt.Println("长度为空！")
	} else {
		fmt.Println("长度不为空！")
	}
}

func ChanTest() {
	startTime := time.Now().UnixMilli()
	done := make(chan int)
	done2 := make(chan int)
	fmt.Println("Main going to call hello go goroutine")
	//hello(done)
	//hello2(done2)
	go hello(done)
	go hello2(done2)
	fmt.Println("Main received data")
	t1 := <-done
	t2 := <-done2
	fmt.Println(t1, t2)
	str := fmt.Sprintf("总耗时%dms", time.Now().UnixMilli()-startTime)
	fmt.Println(str)
}

//func hello(done chan)
func hello(done chan int) {
	fmt.Println("hello go routine is going to sleep")
	done <- 0
	return
	time.Sleep(4 * time.Second)
	fmt.Println("hello go routine awake and going to write to done")

}

func hello2(done chan int) {
	fmt.Println("hello2 go routine is going to sleep")
	time.Sleep(4 * time.Second)
	fmt.Println("hello2 go routine awake and going to write to done")
	done <- 1
}

func goCron() {
	scheduler := gocron.NewScheduler(time.UTC)
	job1 := scheduler.Every(10).Seconds()
	job1.Do(task1)
	//job2 := scheduler.Every(1).Seconds()
	//job2.Do(task2)
	//job3 := scheduler.Every(1).Seconds()
	//job3.Do(task3)
	//job2.Do(taskWithParams, 1, "hello")
	//// 设置crontab字符串格式
	//job4 := scheduler.Cron("0/10 * * * *")
	//job4.Do(task1)
	//scheduler.StartBlocking()
	//可并发运行多个任务
	//注意 interval>1时调用sAPi
	//gocron.Every(2).Seconds().Do(task)
	//gocron.Every(1).Second().Do(taskWithParams, 1, "hi")
	////在cron所有操作最后调用 start函数，否则start之后调用的操作无效不执行
	//<-gocron.Start()

	//s := gocron.NewScheduler(time.UTC)
	//
	//// 每隔多久
	//s.Every(1).Seconds().Do(task1)
	//s.Every(1).Minutes().Do(task1)
	//s.Every(1).Hours().Do(task1)
	//s.Every(1).Days().Do(task1)
	//s.Every(1).Weeks().Do(task1)
	//
	//// 每周几
	//s.Every(1).Monday().Do(task1)
	//s.Every(1).Thursday().Do(task1)
	//
	//// 每天固定时间
	//s.Every(1).Days().At("10:30").Do(task1)
	//s.Every(1).Monday().At("18:30").Do(task1)
	//s.Every(1).Tuesday().At("18:30:59").Do(task1)
	//
	//// 设置crontab字符串格式
	//s.Cron("*/1 * * * *").Do(task1)
	//
	scheduler.StartBlocking()
}

func task1() {
	fmt.Println(time.Now(), "I am runnning task1.")
}

func task2() {
	fmt.Println(time.Now(), "I am runnning task2.")
}

func task3() {
	fmt.Println(time.Now(), "I am runnning task3.")
}

func taskWithParams(a int, b string) {
	fmt.Println(a, b)
}

// timeConser @description: 时间测试
func timeConser() {
	nTime := time.Now()
	fmt.Println(nTime)
	yesTime := nTime.AddDate(0, 0, -2)
	fmt.Println(yesTime.Format("2006-01-02 15:04"))
	//nTime = nTime.Add(+time.Minute * 10)
	fmt.Println(nTime.Add(-time.Minute * 10))
}

func isNullTest() {
	fmt.Println("-----------指针类型判空实验----------")
	var g *day1.DeviceGroup
	var ip *int
	var sp *string
	var iSlice []int
	CheckTypeByReflectNil(g)
	CheckTypeByReflectNil(ip)
	CheckTypeByReflectNil(sp)
	CheckTypeByReflectNil(iSlice)
	fmt.Println("-----------基础类型判空实验----------")
	var a int
	var b bool
	b = false
	var c float64
	var d byte
	var e string
	var f day1.DeviceGroup
	CheckTypeByReflectZero(a)
	CheckTypeByReflectZero(b)
	CheckTypeByReflectZero(c)
	CheckTypeByReflectZero(d)
	CheckTypeByReflectZero(e)
	CheckTypeByReflectZero(f)
}

func CheckTypeByReflectNil(arg interface{}) {
	if reflect.ValueOf(arg).IsNil() { //利用反射直接判空，指针用isNil
		// 函数解释：isNil() bool	判断值是否为 nil
		// 如果值类型不是通道（channel）、函数、接口、map、指针或 切片时发生 panic，类似于语言层的v== nil操作
		fmt.Printf("反射判断：数据类型为%s,数据值为：%v,nil：%v \n",
			reflect.TypeOf(arg).Kind(), reflect.ValueOf(arg), reflect.ValueOf(arg).IsValid())
	}
}

func CheckTypeByReflectZero(arg interface{}) {
	if reflect.ValueOf(arg).IsZero() { //利用反射直接判空，基础数据类型用isZero
		fmt.Printf("反射判断：数据类型为%s,数据值为：%v,nil：%v \n",
			reflect.TypeOf(arg).Kind(), reflect.ValueOf(arg), reflect.ValueOf(arg).IsValid())
	}
}

func Strconv() {
	var a int = 3
	var b int = 7
	fmt.Println(a / b)                                                            // 结果为0，不符合预期
	num0, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", a/b), 64)                   // 保留2位小数
	fmt.Println(num0)                                                             // 结果为0，不符合预期
	num1 := fmt.Sprintf("%.2f", float64(a)/float64(b))                            // 保留2位小数
	fmt.Println(string(num1))                                                     // 0.43
	num2, _ := strconv.ParseFloat(fmt.Sprintf("%.5f", float64(a)/float64(b)), 64) // 保留5位小数
	fmt.Println(num2)                                                             // 0.42857
}

func IntListToInString(list interface{}) string {
	var str strings.Builder
	str.WriteString("(")
	for index, s := range list.([]interface{}) {
		str.WriteString("'")
		str.WriteString(fmt.Sprintf("%v", s))
		str.WriteString("'")
		if index+1 != len(list.([]interface{})) {
			str.WriteString(",")
		}
	}
	str.WriteString(")")
	return str.String()
}

func demo(src []int) {
	for i := 0; i < len(src); i++ {
		first := src[i]
		temp := make([]int, 0)
		temp = append(temp, src[:i]...)
		temp = append(temp, src[i+1:]...)
		order([]int{first}, temp)
	}
}
func order(s []int, src []int) {
	if len(src) <= 1 {
		fmt.Println(s, src)
		return
	}
	firt := src[0]
	for i := 0; i < len(src); i++ {
		firt, src[i] = src[i], firt
		temp := make([]int, 0)
		temp = append(temp, src[1:]...)
		s_demo := make([]int, len(s))
		s_demo = s
		dd := make([]int, 0)
		dd = append(s_demo, firt)
		order(dd, temp)
	}
}

func product(sets ...[]interface{}) [][]interface{} {
	lens := func(i int) int { return len(sets[i]) }
	product := [][]interface{}{}
	for ix := make([]int, len(sets)); ix[0] < lens(0); nextIndex(ix, lens) {
		var r []interface{}
		for j, k := range ix {
			r = append(r, sets[j][k])
		}
		product = append(product, r)
	}
	return product
}

func nextIndex(ix []int, lens func(i int) int) {
	for j := len(ix) - 1; j >= 0; j-- {
		ix[j]++
		if j == 0 || ix[j] < lens(j) {
			return
		}
		ix[j] = 0
	}
}
func IsExitMap() {
	mapTest := make(map[int]string, 0)
	mapTest[1] = "true1"
	mapTest[2] = "true2"
	mapTest[3] = "true3"
	mapTest[4] = "true4"
	if ii, ok := mapTest[12]; !ok {
		fmt.Println("11", ii, ok)
	} else {
		fmt.Println("22", ii, ok)
	}
}

func GetClientIp() string {
	var str string
	netInterfaces, err := net.Interfaces()
	if err != nil {
		fmt.Println("net.Interfaces failed, err:", err.Error())
		return str
	}
	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()
			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						var tempStr = ipnet.IP.String()
						var strList = strings.Split(tempStr, ".")
						if "1" != strList[len(strList)-1] {
							str = tempStr
						}
					}
				}
			}
		}
	}
	return str
}

func inchannel(kV sync.Map, str string) {
	if v, ok := kV.Load(str); ok {
		fmt.Println("sasasasa")
		time.Sleep(9 * time.Second)
		if vv, ok2 := v.(chan int); ok2 {
			vv <- 200
		}
	}
}
