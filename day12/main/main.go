package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"
)

type QueryAlarmImgInfosReq struct {
	OriginalImage   string `json:"original_image" form:"original_image"`                                      //原图
	AlarmImage      string `json:"alarm_image" form:"alarm_image" binding:"required"`                         //告警图片
	CompressedImage string `json:"compressed_image" form:"compressed_image"`                                  //压缩图
	WorkId          string `json:"work_id" form:"work_id"`                                                    //作业ID
	DeviceId        string `json:"device_sn" form:"device_sn" binding:"required"`                             //设备ID
	ChannelID       int    `json:"channel_id" form:"channel_id" binding:"required"`                           //通道ID
	EventName       string `json:"event_name" form:"event_name" binding:"required"`                           // event_name 未戴安全帽
	Event           string `json:"event" form:"event" binding:"required"`                                     // event helmet
	WarnLevel       int    `json:"warn_level" form:"warn_level"`                                              //告警等级0、1、2——————1、2、3;
	IsManualAlarm   int    `json:"is_manual_alarm" form:"is_manual_alarm"`                                    //是否是手动添加的告警
	Timestamp       int64  `json:"timestamp" form:"timestamp" binding:"required,gt=1000000000,lt=2000000000"` //告警秒时间戳，必填，范围1000000000-2000000000
	AiResult        string `json:"ai_result" form:"ai_result"`                                                //告警AI分析结果
	MediaUrl        string `json:"media_url" form:"media_url"`
	ShipAlarmId     int    `json:"ship_alarm_id" form:"ship_alarm_id"` //端侧ship告警id
}

func main() {
	//webcam, _ := gocv.OpenVideoCapture(0)
	//window := gocv.NewWindow("Hello")
	//img := gocv.NewMat()
	//
	//for {
	//	webcam.Read(&img)
	//	window.IMShow(img)
	//	window.WaitKey(1)
	//}
	req := QueryAlarmImgInfosReq{
		DeviceId:  "0201010123040001",
		ChannelID: 5,
		AiResult:  "[{\\\"algo_type\\\":3,\\\"attributes\\\":[{\\\"attribute_id\\\":120,\\\"attribute_name\\\":\\\"sleepy_post\\\",\\\"score\\\":0.13952641189098358,\\\"value\\\":0}],\\\"child\\\":[],\\\"feature\\\":{\\\"data\\\":\\\"\\\",\\\"length\\\":0},\\\"height\\\":342,\\\"keypoints\\\":[],\\\"score\\\":0.81005859375,\\\"track_id\\\":380818,\\\"type_id\\\":1,\\\"type_name\\\":\\\"person\\\",\\\"width\\\":255,\\\"x\\\":616,\\\"y\\\":651},{\\\"algo_type\\\":3,\\\"attributes\\\":[{\\\"attribute_id\\\":120,\\\"attribute_name\\\":\\\"sleepy_post\\\",\\\"score\\\":0.011508950963616371,\\\"value\\\":1}],\\\"child\\\":[],\\\"feature\\\":{\\\"data\\\":\\\"\\\",\\\"length\\\":0},\\\"height\\\":258,\\\"keypoints\\\":[],\\\"score\\\":0.7333984375,\\\"track_id\\\":380717,\\\"type_id\\\":1,\\\"type_name\\\":\\\"person\\\",\\\"width\\\":190,\\\"x\\\":1295,\\\"y\\\":615},{\\\"algo_type\\\":3,\\\"attributes\\\":[{\\\"attribute_id\\\":120,\\\"attribute_name\\\":\\\"sleepy_post\\\",\\\"score\\\":0.03736263886094093,\\\"value\\\":0}],\\\"child\\\":[],\\\"feature\\\":{\\\"data\\\":\\\"\\\",\\\"length\\\":0},\\\"height\\\":201,\\\"keypoints\\\":[],\\\"score\\\":0.728515625,\\\"track_id\\\":380870,\\\"type_id\\\":1,\\\"type_name\\\":\\\"person\\\",\\\"width\\\":147,\\\"x\\\":653,\\\"y\\\":449},{\\\"algo_type\\\":3,\\\"attributes\\\":[{\\\"attribute_id\\\":120,\\\"attribute_name\\\":\\\"sleepy_post\\\",\\\"score\\\":0.05151515081524849,\\\"value\\\":0}],\\\"child\\\":[],\\\"feature\\\":{\\\"data\\\":\\\"\\\",\\\"length\\\":0},\\\"height\\\":280,\\\"keypoints\\\":[],\\\"score\\\":0.650390625,\\\"track_id\\\":380494,\\\"type_id\\\":1,\\\"type_name\\\":\\\"person\\\",\\\"width\\\":187,\\\"x\\\":1178,\\\"y\\\":859},{\\\"algo_type\\\":3,\\\"attributes\\\":[{\\\"attribute_id\\\":120,\\\"attribute_name\\\":\\\"sleepy_post\\\",\\\"score\\\":0.32957392930984497,\\\"value\\\":0}],\\\"child\\\":[],\\\"feature\\\":{\\\"data\\\":\\\"\\\",\\\"length\\\":0},\\\"height\\\":213,\\\"keypoints\\\":[],\\\"score\\\":0.609375,\\\"track_id\\\":380896,\\\"type_id\\\":1,\\\"type_name\\\":\\\"person\\\",\\\"width\\\":122,\\\"x\\\":997,\\\"y\\\":353}]",
	}

	str, err := StructToJsonStr(req)
	if err != nil {
		fmt.Println("err:", err)
	}
	str = strings.ReplaceAll(str, "\"[", "[")
	str = strings.ReplaceAll(str, "]\"", "]")
	str = strings.ReplaceAll(str, "\\", "")
	fmt.Println("len:", len(str))
	byteList := []byte(str)

	//byteList, err := json.MarshalIndent(req, "  ", "  ")
	//if err != nil {
	//	fmt.Println("err:", err)
	//}
	fmt.Println("len:", len(byteList))
}

func StructToJsonStr(eventStruct interface{}) (str string, err error) {
	buf, err := json.Marshal(eventStruct) //格式化编码
	if err != nil {
		log.Errorln(err)
		return
	}
	return string(buf), err
}
