package main

type Job struct {
	ID  int
	Msg string
}

type Worker struct {
	ID         int
	JobQueue   chan Job
	WorkerPool chan chan Job
}

type Dispatcher struct {
	WorkerPool chan chan Job
	MaxWorkers int
}

func NewWorker(id int, workerPool chan chan Job) Worker {
	return Worker{
		ID:         id,
		JobQueue:   make(chan Job),
		WorkerPool: workerPool,
	}
}

func NewDispatcher(maxWorkers int) *Dispatcher {
	return &Dispatcher{
		WorkerPool: make(chan chan Job, maxWorkers),
		MaxWorkers: maxWorkers,
	}
}

func (w Worker) Start() {
	go func() {
		for {
			w.WorkerPool <- w.JobQueue
			select {
			case job := <-w.JobQueue:

			}
		}
	}()
}

func (w Worker) Stop() {

}

func main() {

}

func setGoroutinePool(size int, second int64) {

	for i := 0; i < size; i++ {

	}

}

