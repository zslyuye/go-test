package main

import (
	"fmt"
	"net/http"
)

func main() {
	// 创建一个新的 SeaweedFS 客户端
	client := tt.NewClient("http://localhost:9333")

	// 上传文件
	fileId, err := client.UploadFile("/path/to/file")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(fileId)

	// 下载文件
	resp, err := http.Get(client.LookupFileUrl(fileId))
	if err != nil {
		fmt.Println(err)
		return
	}

	defer resp.Body.Close()

	// 处理响应数据
	// ...

}
