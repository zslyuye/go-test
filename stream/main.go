package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/zeromicro/go-zero/core/fx"
)

func main() {
	//ch := make(chan int)
	//
	//go inputStream(ch)
	//go outputStream(ch)
	//
	//c := make(chan os.Signal, 1)
	//signal.Notify(c, syscall.SIGTERM, syscall.SIGINT)
	//<-c
	group()
}

func inputStream(ch chan int) {
	count := 0
	for {
		ch <- count
		time.Sleep(time.Millisecond * 500)
		count++
	}
}

func outputStream(ch chan int) {
	fx.From(func(source chan<- interface{}) {
		for c := range ch {
			source <- c
		}
	}).Walk(func(item interface{}, pipe chan<- interface{}) {
		count := item.(int)
		pipe <- count
	}).Filter(func(item interface{}) bool {
		itemInt := item.(int)
		if itemInt%2 == 0 {
			return true
		}
		return false
	}).ForEach(func(item interface{}) {
		fmt.Println(item)
	})
}

//group 用 go-zero进行流数据分组
func group() {
	// 例子 按照首字符"g"或者"p"分组，没有则分到另一组
	ss := []string{"golang", "google", "php", "python", "java", "c++"}
	tempList := make([][]string, 0)
	//var any interface{}
	fx.From(func(source chan<- interface{}) {
		for _, s := range ss {
			source <- s
		}
	}).Group(func(item interface{}) interface{} {
		if strings.HasPrefix(item.(string), "g") {
			return "g"
		} else if strings.HasPrefix(item.(string), "p") {
			return "p"
		}
		return ""
	}).ForEach(func(item interface{}) {
		//fmt.Printf("item的数据类型为：%T\n", item)
		var paramSlice []string
		for _, parm := range item.([]interface{}) {
			paramSlice = append(paramSlice, parm.(string))
		}
		tempList = append(tempList, paramSlice)
		//fmt.Println(item)
	})
	fmt.Println(tempList)
}

//func TestInternalStream_Filter(t *testing.T) {
//	// 保留偶数 2,4
//	channel := Just(1, 2, 3, 4, 5).Filter(func(item interface{}) bool {
//		return item.(int)%2 == 0
//	}).channel()
//	for item := range channel {
//		t.Log(item)
//	}
//}

func toResult() {
	//result := make(map[string]string)
	//fx.From(func(source chan<- interface{}) {
	//	for _, item := range data {
	//		source <- item
	//	}
	//}).Walk(func(item interface{}, pipe chan<- interface{}) {
	//	each := item.(*model.ClassData)
	//	class, err := l.rpcLogic.GetClassInfo()
	//	if err != nil {
	//		l.Errorf("get class %s failed: %s", each.ClassId, err.Error())
	//		return
	//	}
	//	students, err := l.rpcLogic.GetUsersInfo(class.ClassId)
	//	if err != nil {
	//		l.Errorf("get students %s failed: %s", each.ClassId, err.Error())
	//		return
	//	}
	//	pipe <- &classObj{
	//		classId: each.ClassId
	//		studentIds: students
	//	}
	//}).ForEach(func(item interface{}) {
	//	o := item.(*classObj)
	//	result[o.classId] = o.studentIds
	//})
}
