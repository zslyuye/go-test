package main

import (
	"bytes"
	"fmt"
	"os/exec"
)

func main() {
	cmd := exec.Command("curl", "--location", "http://172.10.50.56:8000/v1/upload/route_file",
		"--header", "Authorization: eJjd72oLL6fdsByT/8tVgD1EG9RF/ITZB/R1mTs7VMP+Bo6zvCMdOM+2wHUWb/PCRwSYi8D1SDhfjxVwsC8dkv/tKG3qhF1tmh/438/hr+w=",
		"--form", "org_id=\"4\"",
		"--form", "media=@\"/site_resource/10kV122黄周线009号支线_#001.json\"",
		"--form", "line_id=\"47\"",
		"--form", "file_type=\"0\"",
		"--form", "inspection_id=\"1\"",
		"--form", "inspection=\"中飞艾维\"")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		fmt.Println("Error:", err)
	}
	fmt.Println("Response:", out.String())
}

curl --location 'http://172.10.50.56:8000/v1/upload/route_file' \
--header 'Authorization: eJjd72oLL6fdsByT/8tVgD1EG9RF/ITZB/R1mTs7VMP+Bo6zvCMdOM+2wHUWb/PCRwSYi8D1SDhfjxVwsC8dkv/tKG3qhF1tmh/438/hr+w=' \
--form 'org_id="4"' \
--form 'media=@"/C:/Users/Administrator/Desktop/10kV122黄周线(1)/10kV122黄周线/10kV122黄周线_#001.json"' \
--form 'line_id="47"' \
--form 'file_type="0"' \
--form 'inspection_id="1"' \
--form 'inspection="中飞艾维"'
