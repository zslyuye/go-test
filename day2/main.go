package main

import (
	"encoding/json"
	"fmt"
	"time"
)

type Programmer struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main() {
	//JsonConvert()
	//list := make([]string, 0)
	//list = append(list, "111")
	//fmt.Println(ListIsEmpty(list))
	//ss := make(map[string]string, 2)
	//ss["333"] = "5555"
	//ss["444"] = "7777"
	//ss["555"] = "888"
	//state := ListIsEmpty(ss)
	//fmt.Println(state)

	ToTime()
}

//结构体,json相互转换
func JsonConvert() {
	//结构体转json
	var coder = Programmer{Name: "leyanjun", Age: 18}
	data, err := json.Marshal(coder)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(data))

	//json转结构体
	coderJson := `{"Age":18,"name":"乐杨俊"}`
	fmt.Printf("%T\n", coderJson)
	var coder2 Programmer
	err2 := json.Unmarshal([]byte(coderJson), &coder2)
	if err2 != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(coder2)
}

//ListIsEmpty 判断数组是否为空
func ListIsEmpty(list interface{}) bool {
	if nil == list || len(list.([]interface{})) == 0 {
		return true
	}
	return false
}

//ToTime 时间转换
func ToTime() {
	now := time.Now() // 获取当前时间
	fmt.Println("now :=", now)
	timestamp := now.Unix() // 秒级时间戳
	fmt.Println("timestamp :=", timestamp)
	nano := now.UnixNano() // 纳秒时间戳
	fmt.Println("nano :=", nano)
	second := int64(time.Millisecond)
	fmt.Println("second :=", second)
	if timestamp*1000 == nano/second {
		fmt.Println("1相等")
	}
	fmt.Println("timestamp*1000 :=", timestamp*1000)
	fmt.Println("nano/second :=", nano/second)
	if timestamp*1000 == nano%1e6/1e3 {
		fmt.Println("2相等")
	}
	fmt.Println("nano%1e6/1e3 :=", nano%1e6/1e3)
	if nano/second == nano%1e6/1e3 {
		fmt.Println("3相等")
	}

	now2 := time.Now().Unix()
	fmt.Println(now2)
	unix := time.Unix(now2, 0)
	fmt.Println(unix)
}
