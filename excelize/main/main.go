package main

import (
	"fmt"
	"github.com/shuilongzhu/ego"
	"github.com/sirupsen/logrus"
	"github.com/xuri/excelize/v2"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"os"
	"strconv"
	"time"
)

type TB_BASE_USER_ORGANISEUNIT struct {
	ID              string `json:"ID" gorm:"column:ID"`
	USER_ID         string `json:"USER_ID" gorm:"column:USER_ID"`
	ORGANISEUNIT_ID string `json:"ORGANISEUNIT_ID" gorm:"column:ORGANISEUNIT_ID"`
	USER_TYPE       string `json:"USER_TYPE" gorm:"column:USER_TYPE"`
	DISPLAY_INDEX   string `json:"DISPLAY_INDEX" gorm:"column:DISPLAY_INDEX"`
	POSITION_ID     string `json:"POSITION_ID" gorm:"column:POSITION_ID"`
	APPLICATION_ID  string `json:"APPLICATION_ID" gorm:"column:APPLICATION_ID"`
}

type TB_BASE_ORGANISEUNIT struct {
	ID                string    `json:"ID" gorm:"column:ID"`
	APPLICATION_ID    string    `json:"APPLICATION_ID" gorm:"column:APPLICATION_ID"`
	ORGANISEUNIT_CODE string    `json:"ORGANISEUNIT_CODE" gorm:"column:ORGANISEUNIT_CODE"`
	STANDARD_CODE     string    `json:"STANDARD_CODE" gorm:"column:STANDARD_CODE"`
	PARENT_ID         string    `json:"PARENT_ID" gorm:"column:PARENT_ID"`
	ORGANISEUNIT_NAME string    `json:"ORGANISEUNIT_NAME" gorm:"column:ORGANISEUNIT_NAME"`
	SHORT_NAME        string    `json:"SHORT_NAME" gorm:"column:SHORT_NAME"`
	FULL_NAME         string    `json:"FULL_NAME" gorm:"column:FULL_NAME"`
	ORGANISEUNIT_TYPE string    `json:"ORGANISEUNIT_TYPE" gorm:"column:ORGANISEUNIT_TYPE"`
	LOGO_URL          string    `json:"LOGO_URL" gorm:"column:LOGO_URL"`
	TELEPHONE         string    `json:"TELEPHONE" gorm:"column:TELEPHONE"`
	POST_CODE         string    `json:"POST_CODE" gorm:"column:POST_CODE"`
	FAX               string    `json:"FAX" gorm:"column:FAX"`
	ADDRESS           string    `json:"ADDRESS" gorm:"column:ADDRESS"`
	EMAIL             string    `json:"EMAIL" gorm:"column:EMAIL"`
	MANAGER           string    `json:"MANAGER" gorm:"column:MANAGER"`
	DISPLAY_INDEX     string    `json:"DISPLAY_INDEX" gorm:"column:DISPLAY_INDEX"`
	STATUS            int       `json:"STATUS" gorm:"column:STATUS"`
	FIRST_ALPHABET    string    `json:"FIRST_ALPHABET" gorm:"column:FIRST_ALPHABET"`
	ALPHABET          string    `json:"ALPHABET" gorm:"column:ALPHABET"`
	REMARK            string    `json:"REMARK" gorm:"column:REMARK"`
	CREATED_BY        string    `json:"CREATED_BY" gorm:"column:CREATED_BY"`
	CREATED_DATE      time.Time `json:"CREATED_DATE" gorm:"column:CREATED_DATE"`
	MODIFIED_BY       string    `json:"MODIFIED_BY" gorm:"column:MODIFIED_BY"`
	MODIFIED_DATE     time.Time `json:"MODIFIED_DATE" gorm:"column:MODIFIED_DATE"`
}

type TB_BASE_USER struct {
	ID                  string    `json:"ID" gorm:"column:ID"`
	APPLICATION_ID      string    `json:"APPLICATION_ID" gorm:"column:APPLICATION_ID"`
	USER_NAME           string    `json:"USER_NAME" gorm:"column:USER_NAME"`
	EMPLOYEE_ID         string    `json:"EMPLOYEE_ID" gorm:"column:EMPLOYEE_ID"`
	IDCARD              string    `json:"IDCARD" gorm:"column:IDCARD"`
	IDCARD_CODE         string    `json:"IDCARD_CODE" gorm:"column:IDCARD_CODE"`
	USER_CLASS          string    `json:"USER_CLASS" gorm:"column:USER_CLASS"`
	ALIAS               string    `json:"ALIAS" gorm:"column:ALIAS"`
	GENDER              string    `json:"GENDER" gorm:"column:GENDER"`
	ISDN                string    `json:"ISDN" gorm:"column:ISDN"`
	TELEPHONE           string    `json:"TELEPHONE" gorm:"column:TELEPHONE"`
	MOBILE              string    `json:"MOBILE" gorm:"column:MOBILE"`
	FAX                 string    `json:"FAX" gorm:"column:FAX"`
	EMAIL               string    `json:"EMAIL" gorm:"column:EMAIL"`
	OAMAIL              string    `json:"OAMAIL" gorm:"column:OAMAIL"`
	ADDRESS             string    `json:"ADDRESS" gorm:"column:ADDRESS"`
	NATION              string    `json:"NATION" gorm:"column:NATION"`
	NATIVE              string    `json:"NATIVE" gorm:"column:NATIVE"`
	JOINPARTY_TIME      time.Time `json:"JOINPARTY_TIME" gorm:"column:JOINPARTY_TIME"`
	POLITICAL           string    `json:"POLITICAL" gorm:"column:POLITICAL"`
	AGE                 string    `json:"AGE" gorm:"column:AGE"`
	BIRTHDAY            time.Time `json:"BIRTHDAY" gorm:"column:BIRTHDAY"`
	IS_MARRY            string    `json:"IS_MARRY" gorm:"column:IS_MARRY"`
	COMPUTER_LEVEL      string    `json:"COMPUTER_LEVEL" gorm:"column:COMPUTER_LEVEL"`
	POSITION            string    `json:"POSITION" gorm:"column:POSITION"`
	IS_AUTOCHTHONIC     string    `json:"IS_AUTOCHTHONIC" gorm:"column:IS_AUTOCHTHONIC"`
	START_WORKING_DATE  time.Time `json:"START_WORKING_DATE" gorm:"column:START_WORKING_DATE"`
	MYCOMPANY_WORK_DATE time.Time `json:"MYCOMPANY_WORK_DATE" gorm:"column:MYCOMPANY_WORK_DATE"`
	REMARK              string    `json:"REMARK" gorm:"column:REMARK"`
	STATUS              string    `json:"STATUS" gorm:"column:STATUS"`
	ORDER_INDEX         string    `json:"ORDER_INDEX" gorm:"column:ORDER_INDEX"`
	CREATED_BY          string    `json:"CREATED_BY" gorm:"column:CREATED_BY"`
	CREATED_DATE        string    `json:"CREATED_DATE" gorm:"column:CREATED_DATE"`
	MODIFIED_BY         string    `json:"MODIFIED_BY" gorm:"column:MODIFIED_BY"`
	MODIFIED_DATE       string    `json:"MODIFIED_DATE" gorm:"column:MODIFIED_DATE"`
	FIRST_ALPHABET      string    `json:"FIRST_ALPHABET" gorm:"column:FIRST_ALPHABET"`
	ALPHABET            string    `json:"ALPHABET" gorm:"column:ALPHABET"`
	PASSWORD            string    `json:"PASSWORD" gorm:"column:PASSWORD"`
	POSITION_ID         string    `json:"POSITION_ID" gorm:"column:POSITION_ID"`
	WFMAIL_TYPE         string    `json:"WFMAIL_TYPE" gorm:"column:WFMAIL_TYPE"`
	LAST_LOGIN_TIME     time.Time `json:"LAST_LOGIN_TIME" gorm:"column:LAST_LOGIN_TIME"`
}

type QlshAccount struct {
	Id                 int       `json:"id" gorm:"AUTO_INCREMENT; primary_key; column:id"`      //自增ID
	ApplicationAccount string    `json:"application_account" gorm:"column:application_account"` //应用账号
	UserName           string    `json:"user_name" gorm:"column:user_name"`                     //用户姓名
	IdentityAccount    string    `json:"identity_account" gorm:"column:identity_account"`       //统一身份账号
	Department         string    `json:"department" gorm:"column:department"`                   //所属部门
	CreatedTime        time.Time `json:"created_time" gorm:"column:created_time"`               //创建时间
	UpdateTime         time.Time `json:"update_time" gorm:"column:update_time"`                 //更新时间
}

type UserInfo struct {
	Id             string    `gorm:"primary_key;column:id" json:"id"`               //用户id
	Number         string    `gorm:"column:number" json:"number"`                   //手机号
	Password       string    `gorm:"column:password" json:"password"`               //密码
	UserName       string    `gorm:"column:user_name" json:"user_name"`             //用户名
	RoleId         int       `gorm:"column:role_id" json:"role_id"`                 //角色id;1:管理员;2:部门主管;3:成员
	DepartmentId   int       `gorm:"column:department_id" json:"department_id"`     //部门id
	DepartmentName string    `gorm:"column:department_name" json:"department_name"` //部门名称
	Position       string    `gorm:"column:position" json:"position"`               //职位
	Sex            string    `gorm:"column:sex" json:"sex"`                         //性别
	UserState      int       `gorm:"column:user_state" json:"user_state"`           //用户状态;1:正常;2:已禁用
	Note           string    `gorm:"column:note" json:"note"`                       //备注信息
	CreatedTime    time.Time `gorm:"column:created_time" json:"created_time"`       //创建时间
	UpdatedTime    time.Time `gorm:"column:updated_time" json:"updated_time"`       //修改时间
}

type ProjectInfo struct {
	Id                 int       `json:"id" gorm:"AUTO_INCREMENT;primary_key;column:id"`
	ProjectName        string    `json:"project_name" gorm:"column:project_name"`
	ProjectType        int       `json:"project_type" gorm:"column:project_type"` //项目类型(场景);1:安监;2:无人机;3:压缩;4:其他
	HesiBusiness       string    `json:"hesi_business" gorm:"column:hesi_business"`
	BusinessCode       string    `json:"business_code" gorm:"column:business_code"`
	ContractNumber     string    `json:"contract_number" gorm:"column:contract_number"`
	LeaderName         string    `json:"leader_name" gorm:"column:leader_name"`
	CustomerName       string    `json:"customer_name" gorm:"column:customer_name"`
	SalessName         string    `json:"saless_name" gorm:"column:saless_name"`
	ProjectManagerName string    `json:"project_manager_name" gorm:"column:project_manager_name"`
	ProjectStatus      int       `json:"project_status" gorm:"column:project_status"` //项目状态;1:进行中;2:已归档 默认为进行中
	CreatedTime        time.Time `json:"created_time" gorm:"column:created_time"`
	UpdatedTime        time.Time `json:"updated_time" gorm:"column:updated_time"`
}

type MonthWeekInfo struct {
	Id          int       `json:"id" gorm:"AUTO_INCREMENT;primary_key;column:id"`
	StartTime   string    `json:"start_time" gorm:"column:start_time"`
	EndTime     string    `json:"end_time" gorm:"column:end_time"`
	Week        int       `json:"week" gorm:"column:week"`
	Year        int       `json:"year" gorm:"column:year"`
	Month       int       `json:"month" gorm:"column:month"`
	CreatedTime time.Time `json:"created_time" gorm:"column:created_time"`
	UpdatedTime time.Time `json:"updated_time" gorm:"column:updated_time"`
}

var mysqlClient *gorm.DB

func init() {
	if err := connectMysql(); nil != err { //初始化mysqlClient
		logrus.Errorln("mysql初始化失败 err:", err)
	}
}

// ConnectMysql
// @description:连接mysql
func connectMysql() error {

	// 用户名:密码@tcp(IP:port)/数据库?charset=utf8mb4&parseTime=True&loc=Local
	dataSourceName := "maicro:Maicro@2314@tcp(172.10.50.239:3306)/maicro_oa?charset=utf8mb4&parseTime=True&loc=Local"

	// 连接额外配置信息
	gormConfig := gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			//TablePrefix:   MysqlConfigInstance.TablePre, //表前缀
			SingularTable: true, //使用单数表名，启用该选项时，`User` 的表名应该是 `user`而不是users
		},
	}
	// 打印SQL设置
	if true {
		slowSqlTime, err := time.ParseDuration("50ms")
		if nil != err {
			logrus.Errorln("打印SQL设置失败：", err)
			return err
		}
		loggerNew := logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
			SlowThreshold: slowSqlTime, //慢SQL阈值
			LogLevel:      logger.Info,
			Colorful:      true, // 彩色打印开启
		})
		gormConfig.Logger = loggerNew
	}
	var err error
	// 建立连接
	mysqlClient, err = gorm.Open(mysql.New(mysql.Config{
		DSN:                       dataSourceName, // DSN data source name
		DefaultStringSize:         256,            // string 类型字段的默认长度
		DisableDatetimePrecision:  true,           // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,           // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,           // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false,          // 根据当前 MySQL 版本自动配置
	}), &gormConfig)
	if nil != err {
		logrus.Errorln("mySQL建立连接失败：", err)
		return err
	}
	// 设置连接池信息
	sqlDB, err2 := mysqlClient.DB()
	if nil != err2 {
		logrus.Errorln("mySQL设置连接池信息失败：", err2)
		return err2
	}
	// 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxIdleConns(20)
	// 设置打开数据库连接的最大数量
	sqlDB.SetMaxOpenConns(200)
	// 设置了连接可复用的最大时间
	duration, err3 := time.ParseDuration("1h")
	if nil != err3 {
		logrus.Errorln("mySQL设置连接可复用的最大时间失败：", err3)
		return err3
	}
	sqlDB.SetConnMaxLifetime(duration)
	//打印SQL配置信息
	//marshal, _ := json.Marshal(db.Stats())
	//fmt.Printf("数据库配置: %s \n", marshal)
	return nil
}

func main() {
	//addQlshAccount()
	//addMaicroAccount()
	//addProject()
	//addMonthWeekInfo()
	userInfoExcel()
}

func addTB_BASE_USER() {
	f, err := excelize.OpenFile("qlsh.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// 获取工作表中指定单元格的值
	cell, err := f.GetCellValue("TB_BASE_USER", "B2")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cell)
	// 获取 TB_BASE_USER 上所有单元格
	rows, err := f.GetRows("TB_BASE_USER")
	tB_BASE_USERs := make([]TB_BASE_USER, 0)
	for i := 1; i < len(rows); i++ {
		temp := TB_BASE_USER{
			ID:                  rows[i][1],
			APPLICATION_ID:      rows[i][2],
			USER_NAME:           rows[i][3],
			EMPLOYEE_ID:         rows[i][4],
			IDCARD:              rows[i][5],
			IDCARD_CODE:         rows[i][6],
			USER_CLASS:          rows[i][7],
			ALIAS:               rows[i][8],
			GENDER:              rows[i][9],
			ISDN:                rows[i][10],
			TELEPHONE:           rows[i][11],
			MOBILE:              rows[i][12],
			FAX:                 rows[i][13],
			EMAIL:               rows[i][14],
			OAMAIL:              rows[i][15],
			ADDRESS:             rows[i][16],
			NATION:              rows[i][17],
			NATIVE:              rows[i][18],
			JOINPARTY_TIME:      time.Now(),
			POLITICAL:           rows[i][20],
			AGE:                 rows[i][21],
			BIRTHDAY:            time.Now(),
			IS_MARRY:            rows[i][23],
			COMPUTER_LEVEL:      rows[i][24],
			POSITION:            rows[i][25],
			IS_AUTOCHTHONIC:     rows[i][26],
			START_WORKING_DATE:  time.Now(),
			MYCOMPANY_WORK_DATE: time.Now(),
			REMARK:              rows[i][29],
			STATUS:              rows[i][30],
			ORDER_INDEX:         rows[i][31],
			CREATED_BY:          rows[i][32],
			CREATED_DATE:        rows[i][33],
			MODIFIED_BY:         rows[i][34],
			MODIFIED_DATE:       rows[i][35],
			FIRST_ALPHABET:      rows[i][36],
			ALPHABET:            rows[i][37],
			PASSWORD:            rows[i][38],
			POSITION_ID:         rows[i][39],
			WFMAIL_TYPE:         rows[i][40],
			LAST_LOGIN_TIME:     time.Now(),
		}
		tB_BASE_USERs = append(tB_BASE_USERs, temp)

		if i%500 == 0 {
			err := mysqlClient.Table("TB_BASE_USER").Create(&tB_BASE_USERs).Error
			if nil != err {
				logrus.Errorln("执行失败 err:", err)
				return
			}
			tB_BASE_USERs = make([]TB_BASE_USER, 0)
		}
	}
	err = mysqlClient.Table("TB_BASE_USER").Create(&tB_BASE_USERs).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func addTB_BASE_USER_ORGANISEUNIT() {
	f, err := excelize.OpenFile("qlsh.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// 获取 TB_BASE_USER_ORGANISEUNIT 上所有单元格
	rows, err := f.GetRows("TB_BASE_USER_ORGANISEUNIT")
	tB_BASE_USER_ORGANISEUNITs := make([]TB_BASE_USER_ORGANISEUNIT, 0)
	for i := 1; i < len(rows); i++ {
		temp := TB_BASE_USER_ORGANISEUNIT{
			ID:              rows[i][1],
			USER_ID:         rows[i][2],
			ORGANISEUNIT_ID: rows[i][3],
			USER_TYPE:       rows[i][4],
			DISPLAY_INDEX:   rows[i][5],
			POSITION_ID:     rows[i][6],
			APPLICATION_ID:  rows[i][7],
		}
		tB_BASE_USER_ORGANISEUNITs = append(tB_BASE_USER_ORGANISEUNITs, temp)
		if i%500 == 0 {
			err = mysqlClient.Table("TB_BASE_USER_ORGANISEUNIT").Create(&tB_BASE_USER_ORGANISEUNITs).Error
			if nil != err {
				logrus.Errorln("执行失败 err:", err)
				return
			}
			tB_BASE_USER_ORGANISEUNITs = make([]TB_BASE_USER_ORGANISEUNIT, 0)
		}

	}
	err = mysqlClient.Table("TB_BASE_USER_ORGANISEUNIT").Create(&tB_BASE_USER_ORGANISEUNITs).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func addTB_BASE_ORGANISEUNIT() {
	f, err := excelize.OpenFile("qlsh.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// 获取工作表中指定单元格的值
	cell, err := f.GetCellValue("TB_BASE_ORGANISEUNIT", "B2")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cell)
	// 获取 TB_BASE_ORGANISEUNIT 上所有单元格
	rows, err := f.GetRows("TB_BASE_ORGANISEUNIT")
	tB_BASE_ORGANISEUNITs := make([]TB_BASE_ORGANISEUNIT, 0)
	for i := 1; i < len(rows); i++ {
		temp := TB_BASE_ORGANISEUNIT{
			ID:                rows[i][1],
			APPLICATION_ID:    rows[i][2],
			ORGANISEUNIT_CODE: rows[i][3],
			PARENT_ID:         rows[i][4],
			ORGANISEUNIT_NAME: rows[i][5],
			SHORT_NAME:        rows[i][6],
			FULL_NAME:         rows[i][7],
			ORGANISEUNIT_TYPE: rows[i][8],
			CREATED_DATE:      time.Now(),
			MODIFIED_DATE:     time.Now(),
		}
		if 10 == len(rows[i]) {
			temp.DISPLAY_INDEX = rows[i][9]
		} else {
			temp.DISPLAY_INDEX = "0"
		}
		tB_BASE_ORGANISEUNITs = append(tB_BASE_ORGANISEUNITs, temp)
		if i%500 == 0 {
			err := mysqlClient.Table("TB_BASE_ORGANISEUNIT").Create(&tB_BASE_ORGANISEUNITs).Error
			if nil != err {
				logrus.Errorln("执行失败 err:", err)
				return
			}
			tB_BASE_ORGANISEUNITs = make([]TB_BASE_ORGANISEUNIT, 0)
		}

	}
	err = mysqlClient.Table("TB_BASE_ORGANISEUNIT").Create(&tB_BASE_ORGANISEUNITs).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func addQlshAccount() {
	f, err := excelize.OpenFile("云边协同二化账号.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// 获取工作表中指定单元格的值
	cell, err := f.GetCellValue("Sheet1", "B2")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cell)
	// 获取 Sheet1 上所有单元格
	rows, err := f.GetRows("Sheet1")
	qlshAccounts := make([]QlshAccount, 0)
	for i := 1; i < len(rows); i++ {
		if 4 == len(rows[i]) {
			temp := QlshAccount{
				ApplicationAccount: rows[i][0],
				UserName:           rows[i][1],
				IdentityAccount:    rows[i][2],
				Department:         rows[i][3],
				CreatedTime:        time.Now(),
				UpdateTime:         time.Now(),
			}
			qlshAccounts = append(qlshAccounts, temp)
		}
	}
	err = mysqlClient.Table("qlsh_user_info").Create(&qlshAccounts).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func addMaicroAccount() {
	f, err := excelize.OpenFile("D:\\AiRiA\\视觉平台组\\OA工时系统\\研究院花名册.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// 获取工作表中指定单元格的值
	cell, err := f.GetCellValue("Sheet1", "B2")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cell)
	// 获取 Sheet1 上所有单元格
	rows, err := f.GetRows("Sheet1")
	userInfos := make([]UserInfo, 0)
	for i := 1; i < len(rows); i++ {
		status := 0
		err = mysqlClient.Table("user_info").Select("1").Where("number = ?", rows[i][8]).Scan(&status).Error
		if nil != err {
			logrus.Errorln("执行失败 err:", err)
			return
		}

		if 0 == status {
			temp := UserInfo{
				Id:       fmt.Sprintf("1%v", ego.SnowFlakeId()),
				Number:   rows[i][8],
				Password: "2f467d7a488e88769376da41647a3037",
				UserName: rows[i][0],
				RoleId:   3,
				//DepartmentId:   time.Now(),
				DepartmentName: rows[i][3],
				Position:       rows[i][6],
				Sex:            rows[i][5],
				UserState:      1,
				Note:           rows[i][0],
				CreatedTime:    time.Now(),
				UpdatedTime:    time.Now(),
			}
			userInfos = append(userInfos, temp)
		}
	}
	err = mysqlClient.Table("user_info").Create(&userInfos).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func addProject() {
	f, err := excelize.OpenFile("D:\\AiRiA\\视觉平台组\\OA工时系统\\工时统计项目表.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	// 获取工作表中指定单元格的值
	cell, err := f.GetCellValue("工时统计项目表", "B2")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(cell)
	// 获取 Sheet1 上所有单元格
	rows, err := f.GetRows("工时统计项目表")
	projectInfos := make([]ProjectInfo, 0)
	tempMap := map[string]int{
		"安监":  1,
		"无人机": 2,
		"压缩":  3,
		"其他":  4,
	}
	for i := 1; i < len(rows); i++ {
		if len(rows[i]) >= 5 && "" != rows[i][4] {
			temp := ProjectInfo{
				ProjectName:   rows[i][1],
				ProjectType:   tempMap[rows[i][0]],
				HesiBusiness:  rows[i][2],
				BusinessCode:  rows[i][4],
				CustomerName:  rows[i][3],
				ProjectStatus: 1,
				CreatedTime:   time.Now(),
				UpdatedTime:   time.Now(),
			}

			if len(rows[i]) >= 6 {
				temp.ContractNumber = rows[i][5]
				temp.LeaderName = rows[i][6]
				temp.SalessName = rows[i][6]
				temp.ProjectManagerName = rows[i][7]
			}
			projectInfos = append(projectInfos, temp)
		}
	}
	err = mysqlClient.Table("project_info").Create(&projectInfos).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func addMonthWeekInfo() {
	monthWeekInfos := make([]MonthWeekInfo, 0)
	startTime, _ := time.ParseInLocation("2006-01-02", "2024-01-01", time.Local)
	endTime, _ := time.ParseInLocation("2006-01-02", "2024-01-07", time.Local)
	for i := 1; i <= 52; i++ {
		temp := MonthWeekInfo{
			StartTime:   startTime.Format("2006-01-02"),
			EndTime:     endTime.Format("2006-01-02"),
			Week:        i,
			Year:        2024,
			CreatedTime: time.Now(),
			UpdatedTime: time.Now(),
		}
		monthWeekInfos = append(monthWeekInfos, temp)
		startTime = startTime.Add(time.Hour * 24 * 7)
		endTime = endTime.Add(time.Hour * 24 * 7)
	}

	err := mysqlClient.Table("month_week_info").Create(&monthWeekInfos).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
		return
	}
	logrus.Errorln("执行成功！")
}

func userInfoExcel() {
	//1.创建excel
	var f = excelize.NewFile()
	defer func() {
		// 关闭
		if err := f.Close(); err != nil {
			logrus.Errorln(err)
		}
	}()

	f.SetCellValue("Sheet1", "A1", "手机号")
	f.SetCellValue("Sheet1", "B1", "密码")
	f.SetCellValue("Sheet1", "C1", "用户名")
	f.SetCellValue("Sheet1", "D1", "角色名称")
	f.SetCellValue("Sheet1", "E1", "部门名称")
	f.SetCellValue("Sheet1", "F1", "职位")
	f.SetCellValue("Sheet1", "G1", "性别")

	userInfos := make([]UserInfo, 0)
	err := mysqlClient.Table("user_info").Select("number,user_name,role_id,department_name,position,sex").Order("department_id,user_name asc").Scan(&userInfos).Error
	if nil != err {
		logrus.Errorln("用户信息获取失败 err:", err)
		return
	}
	tempMap := map[int]string{1: "管理员", 2: "部门主管", 3: "成员"}
	//4.4总览数据
	for i, info := range userInfos {
		str := strconv.Itoa(i + 2)
		f.SetCellValue("Sheet1", "A"+str, info.Number)
		f.SetCellValue("Sheet1", "B"+str, "Maicro")
		f.SetCellValue("Sheet1", "C"+str, info.UserName)
		f.SetCellValue("Sheet1", "D"+str, tempMap[info.RoleId])
		f.SetCellValue("Sheet1", "E"+str, info.DepartmentName)
		f.SetCellValue("Sheet1", "F"+str, info.Position)
		f.SetCellValue("Sheet1", "G"+str, info.Sex)
	}
	f.SaveAs("OA.xlsx")
}
