//Sql语句
//CREATE TABLE "SYSDBA"."DPI_DEMO"
//(
//"C1" INT,
//"C2" CHAR(20),
//"C3" VARCHAR(50),
//"C4" NUMERIC(7,3),
//"C5" TIMESTAMP(5),
//"C6" CLOB,
//"C7" BLOB) STORAGE(ON "MAIN", CLUSTERBTR) ;

/*该例程实现插入数据，修改数据，删除数据，数据查询等基本操作。*/
package main

// 引入相关包
import (
	"database/sql"
	"fmt"
	_ "github.com/alexbrainman/odbc" // google's odbc driver
	"time"
)

var db *sql.DB
var err error

func main() {
	driverName := "dm"
	dataSourceName := "dm://SYSDBA:SYSDBA@localhost:5236"
	if db, err = connect(driverName, dataSourceName); err != nil {
		fmt.Println(err)
		return
	}
	if err = insertTable(); err != nil {
		fmt.Println(err)
		return
	}
	if err = updateTable(); err != nil {
		fmt.Println(err)
		return
	}
	if err = queryTable(); err != nil {
		fmt.Println(err)
		return
	}
	if err = deleteTable(); err != nil {
		fmt.Println(err)
		return
	}
	if err = disconnect(); err != nil {
		fmt.Println(err)
		return
	}
}

func connect(driverName string, dataSourceName string) (*sql.DB, error) {
	var db *sql.DB
	var err error
	if db, err = sql.Open(driverName, dataSourceName); err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	fmt.Printf("connect to \"%s\" succeed.\n", dataSourceName)
	return db, nil
}

func insertTable() error {

	var sql = `insert into "SYSDBA"."DPI_DEMO"("C1", "C2", "C3", "C4", "C5") VALUES(:1,:2,:3,:4,:5);`

	_, err = db.Exec(sql, "4", "123aaa", "中华书局1", "0.123", time.Now())
	if err != nil {
		return err
	}
	fmt.Println("insertTable succeed")
	return nil
}

func updateTable() error {

	var sql = `update "SYSDBA"."DPI_DEMO" set "C4" = :1 where c1 = :2;`
	if _, err := db.Exec(sql, 222.125, 4); err != nil {
		return err
	}
	fmt.Println("updateTable succeed")
	return nil
}

func queryTable() error {
	var C1 int
	var C2 string
	var C3 string
	var C4 string
	var C5 string

	var sql = `select "C1","C2","C3","C4","C5" from "SYSDBA"."DPI_DEMO";`
	rows, err := db.Query(sql)
	if err != nil {
		return err
	}
	defer rows.Close()
	fmt.Println("queryTable results:")
	for rows.Next() {
		if err = rows.Scan(&C1, &C2, &C3, &C4, &C5); err != nil {
			return err
		}
		fmt.Printf("%v %v %v %v %v\n", C1, C2, C3, C4, C5)
	}
	return nil
}

func deleteTable() error {
	var sql = `delete from "SYSDBA"."DPI_DEMO" where  c1 = :1;`
	if _, err := db.Exec(sql, 4); err != nil {
		return err
	}
	fmt.Println("deleteTable succeed")
	return nil
}

/* 关闭数据库连接 */
func disconnect() error {
	if err := db.Close(); err != nil {
		fmt.Printf("db close failed: %s.\n", err)
		return err
	}
	fmt.Println("disconnect succeed")
	return nil
}
