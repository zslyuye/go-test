package main

//引用包
import (
	"fmt"
	_ "github.com/alexbrainman/odbc" // go官方驱动
	"github.com/go-xorm/xorm"
	"testing"
)

//定义引擎
var (
	engine *xorm.Engine
)

func TestDM(t *testing.T) {
	//数据库连接参数
	params := fmt.Sprintf("driver={DM8 ODBC DRIVER};server=127.0.0.1:5236;database=TEST;uid=SYSDBA;pwd=SYSDBA;charset=utf8")

	/*driver 对应 DM8-ODBC 数据源名称；
	  erver =数据库 IP
	  database = 数据库名称
	  uid = 数据库用户
	  pwd = 数据库用户密码
	  charset = 字符集
	*/

	//创建 Engine 实例连接数据库
	Engine, err := xorm.NewEngine("odbc", params)
	if err != nil {
		fmt.Println("new engine got error:", err)
		return
	}

	//通过 Engine.Ping() 验证数据库是否连接成功
	if err := Engine.Ping(); err != nil {
		fmt.Println("ping got error:", err)
		return
	}

	Engine.ShowSQL(true)
	var test = Test{}
	total, err := Engine.Table("TEST").Count(&test)
	if nil != err {
		fmt.Println(`engine query got error:`, err.Error())
		return
	}
	fmt.Println("total :", total)
	return
}
