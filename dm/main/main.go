package main

import (
	"fmt"
	_ "github.com/alexbrainman/odbc"
	"github.com/go-xorm/xorm"
	"xorm.io/core"
)

type Person struct {
	PersonId int    `xorm:"PERSONID"`
	Sex      string `xorm:"SEX"`
	Name     string `xorm:"NAME"`
	Email    string `xorm:"EMAIL"`
	Phone    string `xorm:"PHONE"`
}

type City struct {
	CityId   string `xorm:"CITY_ID"`
	CityName string `xorm:"CITY_NAME"`
	RegionId int    `xorm:"REGION_ID"`
}

type Test struct {
	Id   string `xorm:"ID"`
	Name string `xorm:"NAME"`
}

func main() {
	Engine, err := xorm.NewEngine("odbc", "driver={DM8 ODBC DRIVER};server=172.10.50.238:5236;database=dm;uid=SYSDBA;pwd=SYSDBA001;charset=utf8")
	if err != nil {
		fmt.Println("new engine got error:", err)
		return
	}
	if err := Engine.Ping(); err != nil {
		fmt.Println("ping got error:", err)
		return
	}

	Engine.SetTableMapper(core.SameMapper{})
	Engine.ShowSQL(true)
	Engine.ShowExecTime(true)
	Engine.SetMaxOpenConns(5)
	Engine.SetMaxIdleConns(5)
	//Engine.SQL("SELECT count(*) FROM TEST.citY")
	var test = Test{}
	total, err := Engine.Table("SYSDBA.TEST").Count(&test)
	if nil != err {
		fmt.Println(`engine query got error:`, err.Error())
		return
	}

	fmt.Println("total count is:", total)
}
