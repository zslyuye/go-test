package main

//
//import (
//	"database/sql"
//	"fmt"
//	_ "github.com/denisenkom/go-mssqldb"
//	gorm2 "github.com/jinzhu/gorm"
//	"gorm.io/driver/sqlserver"
//	"gorm.io/gorm"
//	"testing"
//)
//
//func TestDb(t *testing.T) {
//	// 初始化数据库连接
//	server := "127.0.0.1"
//	port := 5236
//	user := "SYSDBA"
//	password := "SYSDBA"
//	database := "TEST"
//	dsn := fmt.Sprintf("server=%s;port=%d;userid=%s;password=%s;database=%s;", server, port, user, password, database)
//
//	sqlDB, err := sql.Open("mssql", dsn)
//	if err != nil {
//		panic("failed to connect database")
//	}
//
//	db, err := gorm.Open(sqlserver.New(sqlserver.Config{
//		DriverName: "sqlserver",
//		Conn:       sqlDB,
//	}), &gorm.Config{})
//	if err != nil {
//		fmt.Println(err)
//		panic("failed to connect database")
//	}
//	var count int64
//	db.Table("TEST.CITY").Count(&count)
//	fmt.Println(count)
//	// 在此处使用 db 进行数据访问操作
//}
//
//func TestDbA(t *testing.T) {
//	db, err := gorm2.Open("mssql", "sqlserver://SYSDBA:SYSDBA@127.0.0.1:5236?database=TEST")
//	if err != nil {
//		panic("failed to connect database")
//	}
//	defer db.Close()
//
//}
//
////func TestDbB(t *testing.T) {
////	Engine, err := xorm.NewEngine("odbc", "driver={DM8 ODBC DRIVER};server=127.0.0.1:5236;database=TEST;uid=SYSDBA;pwd=SYSDBA;charset=utf8")
////	if err != nil {
////		fmt.Println("new engine got error:", err)
////		return
////	}
////	if err := Engine.Ping(); err != nil {
////		fmt.Println("ping got error:", err)
////		return
////	}
////	adapter := xormadapter.NewAdapter(engine)
////
////	db, err := gorm.Open(mysql.New(mysql.Config{
////		Conn: adapter,
////	}), &gorm.Config{})
////	if err != nil {
////		// 处理错误
////	}
////	db.Logger = logger.Default.LogMode(logger.Silent) // 禁用日志
////	db.DB().SetMaxOpenConns(10)                       // 设置最大打开连接数
////	db.DB().SetMaxIdleConns(5)                        // 设置最大空闲连接数
////
////}
