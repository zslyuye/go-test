package main

import (
	//"database/sql/driver"
	"fmt"
	_ "github.com/alexbrainman/odbc" // go官方驱动
	"github.com/go-xorm/xorm"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"testing"
	//"github.com/go-xorm/core"
	"xorm.io/core"
)

func TestDM1(t *testing.T) {
	// 创建 Xorm 引擎
	//engine, err := xorm.NewEngine("dm", "user:password@tcp(host:port)/database")
	//if err != nil {
	//	// 处理错误
	//}

	engine, err := xorm.NewEngine("odbc", "driver={DM8 ODBC DRIVER};server=127.0.0.1:5236;database=TEST;uid=SYSDBA;pwd=SYSDBA;charset=utf8")
	if err != nil {
		fmt.Println("new engine got error:", err)
		return
	}
	if err := engine.Ping(); err != nil {
		fmt.Println("ping got error:", err)
		return
	}

	// 配置 Xorm 引擎
	engine.SetMapper(core.SameMapper{}) // 使用相同的命名规则
	// 创建 Gorm 数据库连接对象
	db, err := gorm.Open(mysql.New(mysql.Config{
		Conn: engine.DB().DB,
	}), &gorm.Config{})
	if err != nil {
		// 处理错误
	}

	// 配置 Gorm 数据库连接对象
	db.Logger = db.Logger.LogMode(logger.Silent) // 禁用日志
	//db.DB().SetMaxOpenConns(10)                  // 设置最大打开连接数
	//db.DB().SetMaxIdleConns(5)                   // 设置最大空闲连接数

	// 使用 Gorm 进行查询和修改等操作，适配器将负责将它们转换为 Xorm 调用。
	//var users []User
	//db.Find(&users)
	//
	//var user User
	//db.First(&user, 1)
	//
	//db.Create(&User{Name: "Alice", Age: 20})
	//
	//db.Model(&user).Update("Name", "Bob")
	//
	//db.Delete(&user)
}
