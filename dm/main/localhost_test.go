package main

import (
	"fmt"
	_ "github.com/alexbrainman/odbc"
	"github.com/go-xorm/xorm"
	"testing"
	//"xorm.io/core"
)

func TestDbLocalhost(t *testing.T) {
	Engine, err := xorm.NewEngine("odbc", "driver={DM8 ODBC DRIVER};server=127.0.0.1:5236;database=TEST;uid=SYSDBA;pwd=SYSDBA;charset=utf8")
	if err != nil {
		fmt.Println("new engine got error:", err)
		return
	}
	if err := Engine.Ping(); err != nil {
		fmt.Println("ping got error:", err)
		return
	}

	//Engine.SetTableMapper(core.SameMapper{})
	Engine.ShowSQL(true)
	Engine.ShowExecTime(true)
	Engine.SetMaxOpenConns(5)
	Engine.SetMaxIdleConns(5)
	//Engine.SQL("SELECT count(*) FROM TEST.citY")
	var test = Test{}
	total, err := Engine.Table("TEST").Count(&test)
	if nil != err {
		fmt.Println(`engine query got error:`, err.Error())
		return
	}

	fmt.Println("total count is:", total)
}
