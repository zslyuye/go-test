package main

import (
	"fmt"
	"reflect"
	"strconv"
	"time"
)

func main() {
	fmt.Println(strconv.Itoa(-1))
	interfece()
	chanale := make(chan int)
	go a(chanale)
	select {
	case a := <-chanale:
		fmt.Printf("%d", a)
	case <-time.After(5 * time.Second):
		fmt.Println("fail")
		return
	}

}

func method() {
	chanale := make(chan int)
	go a(chanale)
	select {
	case a := <-chanale:
		fmt.Printf("%d", a)
	case <-time.After(5 * time.Second):
		fmt.Println("fail")
		return
	}
}

func a(ch chan int) {
	fmt.Println("a")
	time.Sleep(5 * time.Second)
	ch <- 1
}

func interfece() {
	v := reflect.ValueOf(5)
	i := v.Interface().(int) // 5
	fmt.Println("i is ", i)
}
