package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"
	"strings"
)
import "encoding/json"

func main() {
	//marshalTest()
	createZip("C:\\Users\\Administrator\\Desktop\\11\\333\\7130503142390628352-1-AlarmImage.jpg")
}

//marshalTest 测试json.Marshal()用法
func marshalTest() {
	//Map of students for name and roll number.
	Students := map[string]int{"Rahul": 101, "Rohit": 102, "Virat": 103}
	fmt.Println(Students)
	result, _ := json.Marshal(Students)
	fmt.Println(string(result))
}

func createZip(filename string) {
	// 缓存压缩文件内容
	buf := new(bytes.Buffer)

	// 创建zip
	writer := zip.NewWriter(buf)
	//defer writer.Close()

	// 读取文件内容
	content, _ := ioutil.ReadFile(filepath.Clean(filename))

	// 接收
	f, _ := writer.Create("7130503142390628352-1-AlarmImage.jpg")
	f.Write(content)
	writer.Close()

	filename = strings.TrimSuffix(filename, path.Ext(filename)) + ".zip"
	ioutil.WriteFile(filename, buf.Bytes(), 0644)
}
