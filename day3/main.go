package main

import (
	"fmt"
	"sync"
	"time"
)

// kV 存放数据的并发安全的map
var kV sync.Map

// Set 缓存过期功能实现 类Redis
func Set(key interface{}, value interface{}, exp time.Duration) {
	kV.Store(key, value)
	time.AfterFunc(exp, func() {
		kV.Delete(key)
	})
}

func main() {
	//fmt.Println([]string{"dfdf"})
	var tempMap = map[string]map[string]string{}
	//tempMap = make(map[string]map[string]string)
	token := make(map[string]string)
	token["34"] = "555"
	tempMap["1"] = token
	fmt.Println(tempMap["1"])
	////fmt.Println(tempMap["1"]["34"])
	//if v, ok := tempMap["1"]; ok {
	//	tempMap["1"]["35"] = "898"
	//	tempMap["1"]["36"] = "899"
	//	fmt.Println(v, ok)
	//}
	//for k, v := range tempMap["1"] {
	//	fmt.Println(k, v)
	//}
	//fmt.Println(tempMap["1"])
	//delete(tempMap["2"], "89")
	//fmt.Println(tempMap["1"])
	//fmt.Println("wewe:" + tempMap["1"]["iuuiu"])
	//fmt.Println("erre:" + tempMap["2"]["5454"])
	//tempMap["2"]["5454"] = "78"
	//fmt.Println(tempMap["2"])
	//fmt.Println(time.Now().Unix())
	//// 设置id键值对 过期时间为5s
	//Set("id", "1u2d3g", time.Second*5)
	//// 过4s后查看数据存在
	//time.Sleep(time.Second * 4)
	//fmt.Println(kV.Load("id"))
	//// 过6s后查看数据已经不在了
	//time.Sleep(time.Second * 6)
	//fmt.Println(kV.Load("id"))
	//kV.Store("1", time.Now().Unix())
	//kV.Store("2", time.Now().Unix())
	//kV.Store("3", time.Now().Unix())
	//kV.Range(walk)
	//kV.Range(walk2)
	//fmt.Println("-----------------")
	//kV.Store("3", time.Now().Unix())
	//kV.Store("4", time.Now().Unix())
	//kV.Store("8", 1000000000)
	//if v, ok := kV.Load("8"); 2000000000 == v {
	//	fmt.Println("rtrtrt", ok)
	//} else {
	//	fmt.Println("909009", ok)
	//}
	//fmt.Println(kV.Load("8"))
	//kV.Range(walk2)
}
func walk(key, value interface{}) bool {
	kV.Delete(key.(string))
	fmt.Println(key.(string), value.(int64))
	return true
}

func walk2(key, value interface{}) bool {
	fmt.Println("walk2")
	fmt.Println(key.(string), value.(int64))
	return true
}
