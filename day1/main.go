package day1 //声明所在包

import "fmt" //导入fmt包

func main() { //函数入口，以main为方法
	executePanic()
	fmt.Println("Main block is executed completely...")
	fmt.Println(Name)
	d := new(DeviceGroup)
	fmt.Println(d)
	//使用make函数初始化这个变量,并指定大小(也可以不指定)

	var tt map[string]string
	//使用make函数初始化这个变量,并指定大小(也可以不指定)
	tt = make(map[string]string, 2)
	//存储key ，value
	tt["xiaoming"] = "北京"
	tt["xiaowang"] = "河北"
	fmt.Println(tt["xiaoming"])
	var ss map[string]string
	//使用make函数初始化这个变量,并指定大小(也可以不指定)
	ss = make(map[string]string, 2)
	ss["333"] = "5555"
	ss["444"] = "7777"
	ss["555"] = "888"
	fmt.Println(len(ss))
}

func executePanic() {
	defer func() {
		if errMSg := recover(); errMSg != nil {
			fmt.Println(errMSg)
		}
	}()
	//defer fmt.Println("defer func")
	panic("This is Panic Situation")
	fmt.Println("The function executes Completely")
	d := new(DeviceGroup)
	fmt.Println(d)
}
