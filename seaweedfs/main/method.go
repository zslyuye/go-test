package main

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

func UploadFile() {
	// 打开本地文件
	file, err := os.Open("C:\\Users\\Administrator\\Desktop\\VID20230302170137.mp4")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// 获取文件信息
	//fileInfo, err := file.Stat()
	//if err != nil {
	//	panic(err)
	//}

	// 创建multipart/form-data请求体
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// 写入文件到multipart/form-data请求体
	part, err := writer.CreateFormFile("file", "")
	if err != nil {
		panic(err)
	}
	_, err = io.Copy(part, file)
	if err != nil {
		panic(err)
	}

	err = writer.Close()
	if err != nil {
		panic(err)
	}

	// 指定文件路径
	filePath := "/path/test/"

	// 创建上传请求
	req, err := http.NewRequest("POST", fmt.Sprintf("http://172.10.50.238:8888%s", filePath+"fff.mp4"), body)
	if err != nil {
		panic(err)
	}

	// 设置Content-Type头部信息
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// 发送上传请求
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// 打印上传结果
	fmt.Printf("上传结果：%d\n", resp.StatusCode)
}

func UploadBase64Img() {
	//获取本地文件
	file, err := os.Open("C:\\Users\\Administrator\\Desktop\\HGITF5SL[4DED1{O[07DO@0.png")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	imgByte, _ := ioutil.ReadAll(file)
	// 示例base64格式的图片
	//base64Img := "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAEklEQVR42mNkIAAAAgAB..."
	base64Img := base64.StdEncoding.EncodeToString(imgByte)

	// 解码base64图片数据
	//imgData, err := base64.StdEncoding.DecodeString(strings.Split(base64Img, ",")[1])
	imgData, err := base64.StdEncoding.DecodeString(base64Img)
	if err != nil {
		panic(err)
	}

	// 创建multipart/form-data请求体
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", "")
	if err != nil {
		panic(err)
	}
	_, err = part.Write(imgData)
	if err != nil {
		panic(err)
	}
	err = writer.Close()
	if err != nil {
		panic(err)
	}

	// 指定文件路径
	filePath := "/path/to/file/image.png"

	// 创建上传请求
	req, err := http.NewRequest("POST", fmt.Sprintf("http://172.10.50.238:8888%s", filePath), body)
	if err != nil {
		panic(err)
	}

	// 设置Content-Type头部信息
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// 发送上传请求
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// 打印上传结果
	fmt.Printf("上传结果：%d\n", resp.StatusCode)
}

func UploadBase64Imgs() {
	//获取本地文件
	file1, err := os.Open("C:\\Users\\Administrator\\Desktop\\HGITF5SL[4DED1{O[07DO@0.png")
	if err != nil {
		panic(err)
	}
	defer file1.Close()
	imgByte1, _ := ioutil.ReadAll(file1)
	// 示例base64格式的图片
	//base64Img := "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAEklEQVR42mNkIAAAAgAB..."
	base64Img1 := base64.StdEncoding.EncodeToString(imgByte1)

	//获取本地文件
	file2, err := os.Open("C:\\Users\\Administrator\\Desktop\\捕获.PNG")
	if err != nil {
		panic(err)
	}
	defer file1.Close()
	imgByte2, _ := ioutil.ReadAll(file2)
	// 示例base64格式的图片
	//base64Img := "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAEklEQVR42mNkIAAAAgAB..."
	base64Img2 := base64.StdEncoding.EncodeToString(imgByte2)

	// 示例base64格式的图片列表
	base64Imgs := []string{
		base64Img2,
		base64Img1,
	}

	// 创建multipart/form-data请求体
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	for i, base64Img := range base64Imgs {
		// 解码base64图片数据
		imgData, err := base64.StdEncoding.DecodeString(base64Img)
		if err != nil {
			panic(err)
		}

		// 创建multipart/form-data请求体
		part, err := writer.CreateFormFile(fmt.Sprintf("file%d", i+1), fmt.Sprintf("image%d.jpg", i+1))
		if err != nil {
			panic(err)
		}
		_, err = part.Write(imgData)
		if err != nil {
			panic(err)
		}
	}

	err = writer.Close()
	if err != nil {
		panic(err)
	}

	// 指定文件路径
	filePath := "/path/to/ppp/"

	// 创建上传请求
	req, err := http.NewRequest("POST", fmt.Sprintf("http://172.10.50.238:8888%s", filePath), body)
	if err != nil {
		panic(err)
	}

	// 设置Content-Type头部信息
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// 发送上传请求
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// 打印上传结果
	fmt.Printf("上传结果：%d\n", resp.StatusCode)
}

func Delete() {
	var deleteURL = "http://172.10.50.238:8888/%s?recursive=true" //可以强制删除非空的文件
	// 目标文件夹名称
	dirName := "path"

	// 发送DELETE请求
	deleteURL = fmt.Sprintf(deleteURL, dirName)
	req, err := http.NewRequest(http.MethodDelete, deleteURL, nil)
	if err != nil {
		fmt.Println("创建请求失败：", err)
		return
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("请求失败：", err)
		return
	}
	defer resp.Body.Close()

	fmt.Println("请求状态码：", resp.StatusCode)
}

func Zip() {
	// 声明文件内容
	file1 := []byte("your file content 1")
	file2 := []byte("your file content 2")

	// 压缩两个字节文件流到单个zip文件
	var buf bytes.Buffer
	w := zip.NewWriter(&buf)

	addFileToZip(w, "file1.txt", file1)
	addFileToZip(w, "file2.txt", file2)

	err := w.Close()
	if err != nil {
		fmt.Println("Error closing zip writer:", err)
		return
	}

	// 创建上传请求
	//url := "http://172.10.50.238:8888/zip/my_zip.zip"
	url := "http://172.10.50.238:9333/submit?replication=001&collection=my_collection"
	var requestBody bytes.Buffer
	writer := multipart.NewWriter(&requestBody)
	defer writer.Close()

	// 添加zip文件到请求体
	zipPart, err := writer.CreateFormFile("zip", "my_zip.zip")
	//zipPart, err := writer.CreateFormFile("zip", "")
	if err != nil {
		fmt.Println("Error creating form file:", err)
		return
	}
	zipPart.Write(buf.Bytes())

	// 发送上传请求
	req, err := http.NewRequest("POST", url, &requestBody)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		return
	}
	defer resp.Body.Close()

	// 检查响应并提取上传的文件ID
	if resp.StatusCode == http.StatusOK {
		fileID, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Error reading response body:", err)
			return
		}
		fmt.Println("Zip file uploaded successfully with ID:", string(fileID))
	} else {
		responseBytes, _ := ioutil.ReadAll(resp.Body)
		fmt.Println("Error uploading zip file:", string(responseBytes))
	}
}

func addFileToZip(w *zip.Writer, filename string, fileContent []byte) {
	f, err := w.Create(filename)
	if err != nil {
		fmt.Println("Error creating file in zip:", err)
		return
	}
	_, err = f.Write(fileContent)
	if err != nil {
		fmt.Println("Error writing file to zip:", err)
		return
	}
}
