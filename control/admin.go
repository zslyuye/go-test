package control

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// One godoc
// @Summary      ping example
// @Description  do ping
// @Tags         example
// @Accept       json
// @Produce      plain
// @Success      200  {string}  string  "pong"
// @Failure      400  {string}  string  "ok"
// @Failure      404  {string}  string  "ok"
// @Failure      500  {string}  string  "ok"
// @Router       /1 [get]
func One(c *gin.Context) {
	//单独获取告警视频入参
	file, _ := c.FormFile("ttt")
	c.SaveUploadedFile(file, "D:\\temp\\AiriaCloudLog\\rr.jpg")
	c.String(http.StatusOK, "hello World!1")
}

// Two godoc
// @Summary      ping example
// @Description  do ping
// @Tags         example
// @Accept       json
// @Produce      plain
// @Success      200  {string}  string  "pong"
// @Failure      400  {string}  string  "ok"
// @Failure      404  {string}  string  "ok"
// @Failure      500  {string}  string  "ok"
// @Router       /2 [get]
func Two(c *gin.Context) {
	c.String(http.StatusOK, "hello World!2")
}

// Three godoc
// @Summary      ping example
// @Description  do ping
// @Tags         example
// @Accept       json
// @Produce      plain
// @Success      200  {string}  string  "pong"
// @Failure      400  {string}  string  "ok"
// @Failure      404  {string}  string  "ok"
// @Failure      500  {string}  string  "ok"
// @Router       /3 [get]
func Three(c *gin.Context) {
	c.String(http.StatusOK, "hello World!3")
}
