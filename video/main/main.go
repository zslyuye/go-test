package main

import (
	"crypto/aes"
	"crypto/cipher"
	"fmt"
)

func main() {
	str := "d0816080123120004c11"
	fmt.Println(len(str))

	fmt.Println(str[1:17])
	fmt.Println(str[18:])
	//fmt.Println(time.Unix(0, 0).Format("20060102150405"))
	//// 打开视频文件
	//formatCtx := avformat.AvformatAllocContext()
	//if avformat.AvformatOpenInput(&formatCtx, "input.mp4", nil, nil) < 0 {
	//	fmt.Println("无法打开视频文件")
	//	return197001010800
	//}
	//
	//// 获取视频流信息
	//if avformat.AvformatFindStreamInfo(formatCtx, nil) < 0 {
	//	fmt.Println("无法获取视频流信息")
	//	return
	//}
	//
	//// 找到视频流
	//videoStreamIndex := -1
	//for i := 0; i < int(formatCtx.NbStreams()); i++ {
	//	if formatCtx.Streams()[i].CodecParameters().CodecType() == avformat.AVMEDIA_TYPE_VIDEO {
	//		videoStreamIndex = i
	//		break
	//	}
	//}
	//
	//// 找到视频解码器
	//codecParameters := formatCtx.Streams()[videoStreamIndex].CodecParameters()
	//codecID := codecParameters.CodecId()
	//codec := avcodec.AvcodecFindDecoder(codecID)
	//if codec == nil {
	//	fmt.Println("无法找到视频解码器")
	//	return
	//}
	//
	//// 打开解码器上下文
	//codecContext := avcodec.AvcodecAllocContext3(codec)
	//if codecContext == nil {
	//	fmt.Println("无法打开解码器上下文")
	//	return
	//}
	//if avcodec.AvcodecParametersToContext(codecContext, codecParameters) < 0 {
	//	fmt.Println("无法将解码器参数转换为解码器上下文")
	//	return
	//}
	//if avcodec.AvcodecOpen2(codecContext, codec, nil) < 0 {
	//	fmt.Println("无法打开解码器")
	//	return
	//}
	//
	//// 读取视频帧
	//packet := avformat.AvPacketAlloc()
	//frame := avutil.AvFrameAlloc()
	//for avformat.AvReadFrame(formatCtx, packet) >= 0 {
	//	if packet.StreamIndex() == videoStreamIndex {
	//		avcodec.AvcodecSendPacket(codecContext, packet)
	//		for avcodec.AvcodecReceiveFrame(codecContext, frame) >= 0 {
	//			// 处理视频帧
	//			fmt.Println("处理视频帧")
	//		}
	//	}
	//	avutil.AvFrameUnref(frame)
	//	avcodec.AvPacketUnref(packet)
	//}
	//
	//// 释放资源
	//avcodec.AvcodecFreeContext(&codecContext)
	//avformat.AvformatCloseInput(&formatCtx)
}

/*
AES CBC 加密解密函数
AES_CBC_Encrypt()  --加密
AES_CBC_Decrypt()  --解密
参数：
    src:           --明文/密文，需要分组填充，每组16Byte
    key:           --秘钥   常用16byte
    iv:            --初始化向量  16byte
*/
//加密
func AES_CBC_Encrypt(src []byte, key []byte, iv []byte) []byte {
	// 创建并返回一个使用AES算法的cipher.Block接口
	block, err := aes.NewCipher(key)
	// 判断是否创建成功
	if err != nil {
		panic(err)
	}
	// 明文组数据填充
	paddingText := PKCS7Padding_Unpadding.PKCS7Padding(src, block.BlockSize())
	// 创建一个密码分组为链接模式的, 底层使用DES加密的BlockMode接口
	blockMode := cipher.NewCBCEncrypter(block, iv)
	// 加密
	dst := make([]byte, len(paddingText))
	blockMode.CryptBlocks(dst, paddingText)
	return dst
}

// 解密
func AES_CBC_Decrypt(src []byte, key []byte, iv []byte) []byte {
	// 创建并返回一个使用DES算法的cipher.Block接口
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	// 创建一个密码分组为链接模式的, 底层使用DES解密的BlockMode接口
	blockMode := cipher.NewCBCDecrypter(block, iv)
	// 解密
	dst := make([]byte, len(src))
	blockMode.CryptBlocks(dst, src)
	// 分组移除
	dst = PKCS7Padding_Unpadding.PKCS7Unpadding(dst, block.BlockSize())
	return dst
}
