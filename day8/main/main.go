package main

import (
	"encoding/json"
	"fmt"
	"go_test/day7/ffty"
)

const (
	TT = 0 + iota*1000
)

const (
	ErrTokenIssueYYYY = 77

	ErrInvalidJsonFormat = TT + iota*1000
	ErrTokenIssue
	ErrTokenTimeOut
	ErrTokenNothing
	ErrTokenOnlyOne
	ErrGormMethodParam
	ErrGormMethodCreate
	ErrGormMethodDelete
	ErrGormMethodSelect
	ErrGormMethodUpdate
)

func main() {
	defer ffty.CatchGoError("main", false)
	child_enums := map[string]string{
		"id":  "3",
		"key": "key_temp",
	}
	child_json, _ := json.Marshal(child_enums)
	child_string := string(child_json)
	fmt.Printf("print mString:%s", child_string)
	//ppp.YYYY()
	fmt.Println(ErrTokenIssueYYYY)
	fmt.Println(TT)
	fmt.Println(ErrInvalidJsonFormat)
	fmt.Println(ErrTokenIssue)
	fmt.Println(ErrTokenTimeOut)
	tt := *new(interface{})
	yy := new(interface{})
	if tt == yy {
		fmt.Println("22")
	}
	if tt == *yy {
		fmt.Println("77")
	}
	ii := new(interface{})
	pp := 1
	*ii = pp
	if tt == ii {
		fmt.Println("22")
	}
	if tt == *ii {
		fmt.Println("77")
	}
	fmt.Println(ii)
	fmt.Println(*ii)
	aa := new(any)
	bb := new(any)
	if aa == bb {
		fmt.Println("22")
	}
	if *aa == *bb {
		fmt.Println("77")
	}
	if tt == aa {
		fmt.Println("22")
	}
	if tt == *aa {
		fmt.Println("77")
	}
}
