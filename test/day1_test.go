package test

import (
	"fmt"
	"path"
	"runtime"
	"testing"
)

//func TestGroup(t *testing.T) {
//	runCheckedTest(t, func(t *testing.T) {
//		var groups [][]int
//		Just(10, 11, 20, 21).Group(func(item interface{}) interface{} {
//			v := item.(int)
//			return v / 10
//		}).ForEach(func(item interface{}) {
//			v := item.([]interface{})
//			var group []int
//			for _, each := range v {
//				group = append(group, each.(int))
//			}
//			groups = append(groups, group)
//		})
//
//		assert.Equal(t, 2, len(groups))
//		for _, group := range groups {
//			assert.Equal(t, 2, len(group))
//			assert.True(t, group[0]/10 == group[1]/10)
//		}
//	})
//}

func TestInternalStream_Filter(t *testing.T) {
	fmt.Println(1)
}

//func TestCreate(t *testing.T) {
//	req := dto.AlarmImagesByCondition{
//		DeviceGroupID: "10003",
//		StartTime:     "2020-11-24",
//		EndTime:       "2022-12-02",
//		Download:      "download",
//	}
//
//	data, _ := json.Marshal(req)
//	//发出请求
//	//post请求提交json数据
//	resp, err := http.Post("http://172.10.50.42:3021/airiacloud/api/data_center/alarmimgs/info/ingroup", "application/json", bytes.NewBuffer([]byte(data)))
//	if err != nil {
//		log.Errorln("删除模型 error:")
//		//return types.StatusZero, err, nil
//	}
//
//	body, _ := ioutil.ReadAll(resp.Body)
//	//Resp := new(dto.QueryDeviceModelEventsResp)
//	log.Println(string(body))
//	//unmerr := json.Unmarshal(body, Resp)
//	//if unmerr != nil {
//	//	log.Println("json.Unmarshal error", unmerr)
//	//	//return types.StatusZero, unmerr, nil
//	//}
//}

func TestName(t *testing.T) {
	fmt.Println(1)
	// ...
}

func TestCaller(t *testing.T) {
	// 打印出getCallerInfo函数自身的信息
	fmt.Println(getCallerInfo(-1))
	// 打印出getCallerInfo函数的调用者的信息
	fmt.Println(getCallerInfo(1))
}

func getCallerInfo(skip int) (info string) {
	pc, file, lineNo, ok := runtime.Caller(skip)
	if !ok {

		info = "runtime.Caller() failed"
		return
	}
	funcName := runtime.FuncForPC(pc).Name()
	fileName := path.Base(file) // Base函数返回路径的最后一个元素
	return fmt.Sprintf("FuncName:%s, file:%s, line:%d ", funcName, fileName, lineNo)
}
