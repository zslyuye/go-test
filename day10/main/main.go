package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	ChannelStatusEnum := map[string]map[string]int{}
	ChannelStatusEnum["1111"] = make(map[string]int, 0)
	ChannelStatusEnum["1111"]["1"] = 1
	ChannelStatusEnum["1111"]["2"] = 1
	ChannelStatusEnum["1111"]["3"] = 1
	ChannelStatusEnum["1111"]["4"] = 1
	for channelId, _ := range ChannelStatusEnum["1111"] {
		ChannelStatusEnum["1111"][channelId] = 2 //通道状态更新为离线
	}
	list := strings.Split("2323223-1-是滴是滴是滴是滴", "-")
	fmt.Println(list)
	s := []int{1, 3, 2, 4, 7, 56, 7, 9, 12, 34}

	index := sort.SearchInts(s, 7)

	fmt.Println(index)

	index2 := sort.Search(len(s), func(i int) bool {
		return s[i] == 346
	})

	fmt.Println(index2)

	//SortEnum := map[string]string{}

	var SortEnum = map[string]string{}

	SortEnum["11"] = "333"

	fmt.Println(SortEnum)

}
