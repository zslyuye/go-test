package main

import (
	"fmt"
	"time"
)

func main() {
	unbufferedChan()
}

func unbufferedChan() {
	c1 := make(chan string, 2)
	go func() {
		fmt.Println("the will send data to chan")
		time.Sleep(time.Second * 3)
		c1 <- "string"
		fmt.Println("the will sended data to chan")
		//time.Sleep(time.Second * 3)
		//c1 <- "string1"
		//fmt.Println("the will sended data to chan3")
	}()

	time.Sleep(time.Second * 5)
	fmt.Println("the will sended data to chan 2")
	// c1是无缓冲chan，此次会阻塞等待c1写入数据
	s1, ok := <-c1
	fmt.Println("receive data with", s1, ok)
	time.Sleep(time.Second * 3)
	//s1, ok = <-c1
	//fmt.Println("receive data with2", s1, ok)
}
