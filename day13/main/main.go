package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
	"time"
)

func main() {
	//crypto.AESDecryptCBC()
	//
	//cryptor := yiigo.NewCBCCrypto([]byte(config.ENV.SecretKey), []byte(config.ENV.SecretIv), yiigo.AES_PKCS5)
	//yiigo.De
	fmt.Println("arere")
	fmt.Println([]byte("arere"))
	fmt.Println(14 % 4)

	orig := "hello world"
	key := "this is a 16 byte key333"

	fmt.Println("原文：", orig)

	//oq0HS7KfqE6mv7f8cxRkUw

	encryptCode := AesEncrypt(orig, key)
	fmt.Println("密文：", encryptCode)

	decryptCode := AesDecrypt(encryptCode, key)
	fmt.Println("解密结果：", decryptCode)
}

func StrToTimeA(timeStr string) time.Time {
	t, err := time.ParseInLocation("2006-01", timeStr, time.Local)
	if nil != err {
		fmt.Println("StrToTimeA(),err:", err)
		return time.Now()
	}
	return t
}

func AesEncrypt(orig string, key string) string {
	// 转成字节数组
	origData := []byte(orig)
	k := []byte(key)

	// 分组秘钥
	block, err := aes.NewCipher(k)
	if err != nil {
		panic(fmt.Sprintf("key 长度必须 16/24/32长度: %s", err.Error()))
	}
	// 获取秘钥块的长度
	blockSize := block.BlockSize()
	// 补全码
	origData = PKCS7Padding(origData, blockSize)
	// 加密模式
	blockMode := cipher.NewCBCEncrypter(block, k[:blockSize])
	// 创建数组
	cryted := make([]byte, len(origData))
	// 加密
	blockMode.CryptBlocks(cryted, origData)
	//使用RawURLEncoding 不要使用StdEncoding
	//不要使用StdEncoding  放在url参数中回导致错误
	return base64.RawURLEncoding.EncodeToString(cryted)
	//return string(cryted)

}

func AesDecrypt(cryted string, key string) string {
	//使用RawURLEncoding 不要使用StdEncoding
	//不要使用StdEncoding  放在url参数中回导致错误
	crytedByte, _ := base64.RawURLEncoding.DecodeString(cryted)
	k := []byte(key)

	// 分组秘钥
	block, err := aes.NewCipher(k)
	if err != nil {
		panic(fmt.Sprintf("key 长度必须 16/24/32长度: %s", err.Error()))
	}
	// 获取秘钥块的长度
	blockSize := block.BlockSize()
	// 加密模式
	blockMode := cipher.NewCBCDecrypter(block, k[:blockSize])
	// 创建数组
	orig := make([]byte, len(crytedByte))
	// 解密
	blockMode.CryptBlocks(orig, crytedByte)
	// 去补全码
	orig = PKCS7UnPadding(orig)
	return string(orig)
}

//补码
func PKCS7Padding(ciphertext []byte, blocksize int) []byte {
	padding := blocksize - len(ciphertext)%blocksize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

//去码
func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

//
//// AESEncryptCBC AES-CBC 加密(pkcs#7, 默认填充BlockSize)
//func AESEncryptCBC(key, iv, data []byte, paddingSize ...uint8) (*CipherText, error) {
//	block, err := aes.NewCipher(key)
//	if err != nil {
//		return nil, err
//	}
//
//	if len(iv) != block.BlockSize() {
//		return nil, errors.New("IV length must equal block size")
//	}
//
//	blockSize := block.BlockSize()
//	if len(paddingSize) != 0 {
//		blockSize = int(paddingSize[0])
//	}
//	data = pkcs7padding(data, blockSize)
//
//	bm := cipher.NewCBCEncrypter(block, iv)
//	if len(data)%bm.BlockSize() != 0 {
//		return nil, errors.New("input not full blocks")
//	}
//
//	out := make([]byte, len(data))
//	bm.CryptBlocks(out, data)
//
//	return &CipherText{
//		bytes: out,
//	}, nil
//}
//
//// AESDecryptCBC AES-CBC 解密(pkcs#7)
//func AESDecryptCBC(key, iv, data []byte) ([]byte, error) {
//	block, err := aes.NewCipher(key)
//	if err != nil {
//		return nil, err
//	}
//
//	if len(iv) != block.BlockSize() {
//		return nil, errors.New("IV length must equal block size")
//	}
//
//	bm := cipher.NewCBCDecrypter(block, iv)
//	if len(data)%bm.BlockSize() != 0 {
//		return nil, errors.New("input not full blocks")
//	}
//
//	out := make([]byte, len(data))
//	bm.CryptBlocks(out, data)
//
//	return pkcs7unpadding(out), nil
//}
//
//func pkcs7padding(data []byte, blockSize int) []byte {
//	padding := blockSize - len(data)%blockSize
//	if padding == 0 {
//		padding = blockSize
//	}
//
//	b := bytes.Repeat([]byte{byte(padding)}, padding)
//
//	return append(data, b...)
//}
//
//func pkcs7unpadding(data []byte) []byte {
//	length := len(data)
//	padding := int(data[length-1])
//
//	return data[:(length - padding)]
//}
