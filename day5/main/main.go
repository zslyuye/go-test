// An highlighted block
/*
程序目的：查询起飞城市到目的城市的航班信息
程序版本：go1.14
程序编写：枫叶
编写时间：2020年3月15日*/
package main

import (
	"fmt"
	"strings"
)

const (
	airUrl      = "http://ws.webxml.com.cn/webservices/DomesticAirline.asmx?wsdl"
	contentType = "application/soap+xml; charset=utf-8"
)

func main() { //定义起飞城市，抵达城市，查询时间
	list := []int{1, 2, 3, 4}
	fmt.Printf("%v", list[0:5])
	fmt.Printf("%q\n", strings.Split("a,b,c", ","))
	fmt.Printf("%q\n", strings.Split("a", ","))
	fmt.Printf("%q\n", strings.Split("", ","))
	//startCity := "桂林"
	//lastCity := "厦门"
	//theDate := "2020-3-18"
	//userID := ""
	//fmt.Println("----------航班查询系统---------")
	//AirlineSchedule.ReceiveParam(airUrl,contentType,startCity,lastCity,theDate,userID)
	//bytes, _ := base64.StdEncoding.DecodeString("dHQwM1p1elN1eG9IbGJRMHpYUUVnT3ZLbVlDK1BGYytrZ3RPbEpoRkM2V0RNemRpVEN0aHhreFZRdG52SXl4dA==")
	//fmt.Println(string(bytes))

}
