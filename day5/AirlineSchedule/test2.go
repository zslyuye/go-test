package AirlineSchedule

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// An highlighted block
/*
程序目的：查询起飞城市到目的城市的航班信息
程序版本：go1.14
程序编写：枫叶
编写时间：2020年3月15日*/
package mainimport ("GoAlgorithm/AirlineSchedule""fmt"
)const(airUrl = "http://ws.webxml.com.cn/webservices/DomesticAirline.asmx?wsdl"contentType = "application/soap+xml; charset=utf-8"
)func main(){//定义起飞城市，抵达城市，查询时间startCity := "桂林"lastCity := "厦门"theDate := "2020-3-18"userID := ""fmt.Println("----------航班查询系统---------")
	AirlineSchedule.ReceiveParam(airUrl,contentType,startCity,lastCity,theDate,userID)
}/*
程序目的：设置请求报文
程序版本：go1.14
程序编写：枫叶
编写时间：2020年3月15日*/
package AirlineScheduleimport ("fmt""io/ioutil""net/http""strings""time"
)//对外接口函数
func ReceiveParam(url string,contentType string,startCity string,lastCity string,theDate string,userID string){
	fmt.Println("-------正在查询",startCity,"到",lastCity,"航班信息-------")
	buildRequest(url,contentType,startCity,lastCity,theDate,userID)
}//soap1.2
func buildRequest(url string,contentType string,startCity string,lastCity string,theDate string,userID string){
	req := `<?xml version="1.0" encoding="utf-8"?><soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope"><soap12:Body><getDomesticAirlinesTime xmlns="http://WebXml.com.cn/"><startCity>`+startCity+`</startCity><lastCity>`+lastCity+`</lastCity><theDate>`+theDate+`</theDate><userID>`+userID+`</userID></getDomesticAirlinesTime></soap12:Body></soap12:Envelope>`
	res,err := http.Post(url,contentType,strings.NewReader(req))
	if err != nil{
		fmt.Println("http post err:", err)
		return
	}
	defer res.Body.Close()
	if http.StatusOK != res.StatusCode{
		fmt.Println("WebService soap1.2 request fail, status: %s\n", res.StatusCode)
		return
	}
	response,err := ioutil.ReadAll(res.Body)
	if err!=nil{
		fmt.Println("ioutil ReadAll err:", err)
		return}
	fmt.Println("----------",startCity,"到",lastCity,"航班信息一览----------")
	currentTime := time.Now()
	resData := string(response)
	fmt.Println(currentTime.Format("2006-01-02 15:04:05"),"   response:")
	fmt.Println(resData)
	startFlag := strings.Index(resData,"<Airlines") //字符串起始位置，int型
	endFlag := strings.Index(resData,"</diffgr:diffgram>") //字符串结束位置,int型//
	position := strings.Index(resData[comma:],"</Airlines>")//
	fmt.Println(resData[startFlag:endFlag])
	ReceivXml(resData[startFlag:endFlag],startCity,lastCity,theDate) //同目录下函数引用不需要前缀
}/*
程序目的：解析从webservice服务端返回的xml，同时将记录保存到数据库
程序版本：go1.14
程序编写：枫叶
编写时间：2020年3月15日
*/
package AirlineScheduleimport ("GoAlgorithm/UseMariadb""encoding/xml""fmt""time"_ "github.com/go-sql-driver/mysql"
)
//_ "github.com/go-sql-driver/mysql" 操作数据库一定要引用该包，否则会报invalid memory address or nil pointer dereference错误
//内部属性都要大写字母开头，属性节点的名称变量名固定为XMLName
//定义航空信息struct
type airlinestime struct{Company string `xml:"Company"`AirlineCode string `xml:"AirlineCode"`StartDrome string `xml:"StartDrome"`ArriveDrome string `xml:"ArriveDrome"`StartTime string `xml:"StartTime"`ArriveTime string `xml:"ArriveTime"`Mode string `xml:"Mode"`AirlineStop string `xml:"AirlineStop"`Cycle string `xml:"Week"`
}
//定义xml的struct
type xmlInfo struct{XMLName xml.Name `xml:"Airlines"`//定义要解析内容的最外层标签AirlineTime []airlinestime `xml:"AirlinesTime"`
}func ReceivXml(xmlStr string,startCity string,lastCity string,theDate string) {//fmt.Println(xmlStr)analyzeXml(xmlStr,startCity,lastCity,theDate)
}//解析xml
func analyzeXml(xmlStr string,startCity string,lastCity string,theDate string) {//fmt.Println("test:",startCity,lastCity,theDate)airlineData := xmlInfo{}err := xml.Unmarshal([]byte(xmlStr), &airlineData)if err != nil {fmt.Printf("error: %v", err)return}//fmt.Println(airlineData)//fmt.Println("company:",airlineData.AirlineTime[0].Company)//建立数据连接db,err := UseMariadb.ConDatabase()if err != nil{fmt.Println("error:%v",err)return}//insertSql,err := db.Prepare("insert into airline set company=?,airlinecode=?,startdrome=?,arrivedrome=?,starttime=?,arrivetime=?,airmode=?,airlinestop=?,cycle=?,querydate=?,startcity=?,arrivecity=?,platform=?")insertSql,err := db.Prepare("insert into airline(company,airlinecode,startdrome,arrivedrome,starttime,arrivetime,airmode,airlinestop,cycle,querydate,startcity,arrivecity,platform) values (?,?,?,?,?,?,?,?,?,?,?,?,?)")if err != nil {fmt.Println("error:%v",err)}a := 0 //请求报文返回记录b := 0 //插入数据库的记录currentTime := time.Now()fmt.Println(currentTime.Format("2006-01-02 15:04:05"))for i:=0;i< len(airlineData.AirlineTime);i++{res,_ := insertSql.Exec(airlineData.AirlineTime[i].Company,airlineData.AirlineTime[i].AirlineCode,airlineData.AirlineTime[i].StartDrome,airlineData.AirlineTime[i].ArriveDrome,airlineData.AirlineTime[i].StartTime,airlineData.AirlineTime[i].ArriveTime,airlineData.AirlineTime[i].Mode,airlineData.AirlineTime[i].AirlineStop,airlineData.AirlineTime[i].Cycle,theDate,startCity,lastCity,"Go")lastId,_ := res.LastInsertId()if lastId == 0{b++}fmt.Println("航空公司:",airlineData.AirlineTime[i].Company)fmt.Println("航空编号:",airlineData.AirlineTime[i].AirlineCode)fmt.Println("起飞机场:",airlineData.AirlineTime[i].StartDrome)fmt.Println("抵达机场:",airlineData.AirlineTime[i].ArriveDrome)fmt.Println("起飞时间:",airlineData.AirlineTime[i].StartTime)fmt.Println("抵达时间:",airlineData.AirlineTime[i].ArriveTime)fmt.Println("机型:",airlineData.AirlineTime[i].Mode)fmt.Println("经停:",airlineData.AirlineTime[i].AirlineStop)fmt.Println("飞行周期:",airlineData.AirlineTime[i].Cycle)fmt.Println("起飞城市:",startCity)fmt.Println("抵达城市:",lastCity)fmt.Println("查询时间:",theDate)fmt.Println("操作平台:Go")fmt.Println("---------------------------------------------------")a++}insertSql.Close()UseMariadb.CloseDb(db)fmt.Println("共返回",a,"条记录，插入数据库",b,"条...")
}