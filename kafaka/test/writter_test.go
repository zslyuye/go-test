package test

import (
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"log"
	"testing"
)

func TestWrit(t *testing.T) {
	// 构造一个新的Reader对象
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{"172.10.60.59:9092"},
		Topic:     "my-topic",
		Partition: 0,
		MinBytes:  10e3, // 最小读取字节数
		MaxBytes:  10e6, // 最大读取字节数

	})

	//reader.SetOffset(-1)

	defer reader.Close()
	// 初始化 consumer
	err := reader.SetOffset(kafka.LastOffset)
	if err != nil {
		log.Fatal("Error setting offset: ", err)
	}

	// 从Kafka中读取消息
	for {
		message, err := reader.ReadMessage(context.Background())
		//log.Println(reader.Offset())
		if err != nil {
			log.Fatal("failed to read message:", err)
		}
		// 提交 offset
		err = reader.CommitMessages(context.Background())
		if err != nil {
			log.Fatal("Error committing offsets: ", err)
		}
		fmt.Println("received message:", string(message.Value))
	}
}
