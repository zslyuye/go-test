package test

import (
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"log"
	"testing"
)

func TestRead(t *testing.T) {
	// 设置Kafka连接信息
	conn, err := kafka.DialLeader(context.Background(), "tcp", "172.10.60.59:9092", "my-topic", 0)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}

	defer conn.Close()

	// 发送消息
	message := kafka.Message{
		Key:   []byte("key4"),
		Value: []byte("value4"),
	}

	i, err := conn.WriteMessages(message)
	fmt.Println(i)
	if err != nil {
		log.Fatal("failed to write messages:", err)
	}

	fmt.Println("message sent successfully")
}
