package test

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
)

type Person struct {
	Name string
	Age  int
}

type Tearcher struct {
	Name string
	//Age  int
}

//TestSameStructure 结构体完全相同转换
func TestChangeStructure(t *testing.T) {
	var personstr = `{"name":"one","age":1}`
	// json->struct
	var person Person
	JsonStr2Struct(personstr, &person)
	fmt.Println(person.Name)

	// structA->structB
	var tearcher Tearcher
	Object2Object(person, &tearcher)
	//Struct2Struct(person, &tearcher)
	fmt.Println(tearcher)

	// json -> map
	event, _ := JsonStr2Map(personstr)
	fmt.Println(event["name"], "--", event["age"])

	var personOne = Person{
		Name: "Two",
		Age:  2,
	}
	//	struct -> json
	str, _ := Struct2JsonStr(personOne)
	fmt.Println(str)

	// struct -> map
	struct2Map := Struct2Map(personOne)
	fmt.Println(struct2Map["Name"], "--", struct2Map["Age"])

	var mapDemo = make(map[string]interface{})
	mapDemo["Name"] = "test"
	mapDemo["Age"] = 333
	//	map -> json
	s, _ := Map2JsonStr(mapDemo)
	fmt.Println(s)

	// map -> struct
	var personTwo Person
	Map2Struct(mapDemo, &personTwo)
	fmt.Println(personTwo.Age, "----", personTwo.Name)
}

func TestChangeList(t *testing.T) {
	var jsonBlob = []byte(` [ 
        { "Name" : "Platypus" , "Order" : "Monotremata","Other" : [{ "Age" : 25 , "Type" : 0},{ "Age" : 26 , "Type" : 3}] } , 
        { "Name" : "Quoll" ,  "Order" : "Dasyuromorphia","Other" : [{ "Age" : 27 , "Type" : 1}] } 
    ] `)
	type Animal struct {
		Name  string
		Order string
		Other []struct {
			Age  int
			Type int
		}
	}

	type AnimalT struct {
		Name  string
		Order string
		Other []struct {
			Type int
		}
	}
	var animals []Animal
	animalTs := make([]AnimalT, 0)
	err := json.Unmarshal(jsonBlob, &animals)
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Printf("%+v", animals)

	Object2Object(animals, &animalTs)
	fmt.Printf("%+v", animalTs)
}

/**
jsonstring --> map
*/
func JsonStr2Map(jsonStr string) (event map[string]interface{}, err error) {
	if err = json.Unmarshal([]byte(jsonStr), &event); err != nil {
		panic(err)
	}
	return
}

/**
jsonstring --> struct
*/
func JsonStr2Struct(jsonStr string, eventStruct interface{}) {
	if err := json.Unmarshal([]byte(jsonStr), &eventStruct); err != nil {
		panic(err)
	}
}

/**
map --> struct
*/
func Map2Struct(mapBean map[string]interface{}, eventStruct interface{}) {
	//将 map 转换为指定的结构体
	str, err := Map2JsonStr(mapBean)
	if err != nil {
		fmt.Println("err = ", err)
	}
	JsonStr2Struct(str, &eventStruct)
}

/**
map --> jsonstring
*/

func Map2JsonStr(mapBean map[string]interface{}) (str string, err error) {
	bytes, err := json.Marshal(mapBean)
	if err != nil {
		fmt.Println("err = ", err)
		return
	}
	return string(bytes), err
}

/**
struct -> jsonString
*/
func Struct2JsonStr(eventStruct interface{}) (str string, err error) {
	buf, err := json.Marshal(eventStruct) //格式化编码
	if err != nil {
		fmt.Println("err = ", err)
		return
	}
	return string(buf), err
}

/**
struct -> map
*/
func Struct2Map(obj interface{}) map[string]interface{} {
	//获取参数o的类型
	t := reflect.TypeOf(obj)
	v := reflect.ValueOf(obj)

	field := t.NumField()

	var data = make(map[string]interface{})
	for i := 0; field > i; i++ {

		data[t.Field(i).Name] = v.Field(i).Interface()
	}
	return data
}

/**
structA -> structB
*/
func Struct2Struct(objA, objB interface{}) {
	str, _ := Struct2JsonStr(objA)
	JsonStr2Struct(str, objB)
}

/**
ObjectA -> ObjectB
*/
func Object2Object(objA, objB interface{}) {
	tempStr, err := json.Marshal(objA)
	if err != nil {
		fmt.Println("error:", err)
	}
	errT := json.Unmarshal([]byte(tempStr), objB)
	if errT != nil {
		fmt.Println("error:", errT)
	}
}
