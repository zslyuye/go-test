package test

import (
	"fmt"
	"github.com/glebarez/sqlite"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"os"
	"testing"
	"time"
)

type Collect struct {
	Id          int    `json:"id" gorm:"primary_key; column:id"`
	ChannelId   int    `json:"channel_id" gorm:"column:channel_id"`
	WorkId      int    `json:"work_id" gorm:"column:work_id"`
	MediaFormat int    `json:"media_format" gorm:"column:media_format"`
	MediaUri    string `json:"media_uri" gorm:"column:media_uri"`
	Remarks     string `json:"remarks" gorm:"column:remarks"`
	CreateTime  int64  `json:"create_time" gorm:"column:create_time"`
	ModifyTime  int64  `json:"modify_time" gorm:"column:modify_time"`
}

func TestSqlite(t *testing.T) {
	// 连接额外配置信息
	gormConfig := gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			//TablePrefix:   MysqlConfigInstance.TablePre, //表前缀
			SingularTable: true, //使用单数表名，启用该选项时，`User` 的表名应该是 `user`而不是users
		},
	}
	// 打印SQL设置
	if true {
		slowSqlTime, err := time.ParseDuration("50ms")
		if nil != err {
			logrus.Errorln("打印SQL设置失败：", err)
		}
		loggerNew := logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
			SlowThreshold: slowSqlTime, //慢SQL阈值
			LogLevel:      logger.Info,
			Colorful:      true, // 彩色打印开启
		})
		gormConfig.Logger = loggerNew
	}
	//http://172.10.50.239/data_center/maicroshipdata.db
	//C:\Users\Administrator\Desktop\maicroshipdata.db
	db, err := gorm.Open(sqlite.Open("http://172.10.50.239/data_center/maicroshipdata.db"), &gormConfig)
	if nil != err {
		logrus.Errorln("sqliteInit err:", err)
	}
	//defer db.Close()

	fmt.Println("database connected")

	collects := make([]Collect, 0)
	//db.AutoMigrate(&ChannelInfo{})
	if err := db.Table("collect").Select("*").Scan(&collects).Error; nil != err {
		logrus.Errorln("sqliteInit err:", err)
	}

	if err := db.Table("collect").Where("id = ?", 2524).Updates(map[string]interface{}{"work_id": 3444}).Error; nil != err {
		logrus.Errorln("sqliteInit err:", err)
	}
	logrus.Println("成功")
}
