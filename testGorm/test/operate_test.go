package test

import (
	"github.com/shuilongzhu/ego"
	"github.com/shuilongzhu/ego/ttt"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"log"
	"os"
	"testing"
	"time"
)

var mysqlClient *gorm.DB
var db ego.Gorm

// WorkInfo 作业信息表 work_info
type WorkInfo struct {
	Id          int64     `json:"id" gorm:"primary_key; column:id"`        // ID
	WorkId      int       `json:"work_id" gorm:"column:work_id"`           //作业ID
	WorkName    string    `json:"work_name" gorm:"column:work_name"`       //work_name VARCHAR(128)    '作业名称' ,
	DeviceId    string    `json:"device_id" gorm:"column:device_id"`       //device_id VARCHAR(32)     '设备ID' ,
	ChannelId   int       `json:"channel_id" gorm:"column:channel_id"`     //通道ID
	Worker      string    `json:"worker" gorm:"column:worker"`             //worker VARCHAR(64)     '作业负责人' ,
	WorkDate    string    `json:"work_date" gorm:"column:work_date"`       //work_date DATE     '作业日期' ,
	WorkModel   string    `json:"work_model" gorm:"column:work_model"`     //选择模式、仅录制、手机工服安全帽、动火作业、高空作业等
	StartTime   string    `json:"start_time" gorm:"column:start_time"`     //作业开始时间
	EndTime     string    `json:"end_time" gorm:"column:end_time"`         //作业结束时间
	DeleteTag   int       `json:"delete_tag" gorm:"column:delete_tag"`     //`删除标记 0:正常；1：已删除；' ,
	CreatedTime time.Time `json:"created_time" gorm:"column:created_time"` //`创建时间' ,
	UpdateTime  time.Time `json:"update_time" gorm:"column:update_time"`   //`更新时间' ,
}

// TableName 解决gorm表明映射
func (WorkInfo) TableName() string {
	return "work_info"
}

func init() {
	if err := connectMysql(); nil != err { //初始化mysqlClient
		logrus.Errorln("mysql初始化失败 err:", err)
	}
}

// ConnectMysql
// @description:连接mysql
func connectMysql() error {

	// 用户名:密码@tcp(IP:port)/数据库?charset=utf8mb4&parseTime=True&loc=Local
	dataSourceName := "maicro:Maicro@2314@tcp(172.10.50.239:3306)/cloudbusi?charset=utf8mb4&parseTime=True&loc=Local"

	// 连接额外配置信息
	gormConfig := gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			//TablePrefix:   MysqlConfigInstance.TablePre, //表前缀
			SingularTable: true, //使用单数表名，启用该选项时，`User` 的表名应该是 `user`而不是users
		},
	}
	// 打印SQL设置
	if true {
		slowSqlTime, err := time.ParseDuration("50ms")
		if nil != err {
			logrus.Errorln("打印SQL设置失败：", err)
			return err
		}
		loggerNew := logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
			SlowThreshold: slowSqlTime, //慢SQL阈值
			LogLevel:      logger.Info,
			Colorful:      true, // 彩色打印开启
		})
		gormConfig.Logger = loggerNew
	}
	var err error
	// 建立连接
	mysqlClient, err = gorm.Open(mysql.New(mysql.Config{
		DSN:                       dataSourceName, // DSN data source name
		DefaultStringSize:         256,            // string 类型字段的默认长度
		DisableDatetimePrecision:  true,           // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,           // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,           // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false,          // 根据当前 MySQL 版本自动配置
	}), &gormConfig)
	if nil != err {
		logrus.Errorln("mySQL建立连接失败：", err)
		return err
	}
	// 设置连接池信息
	sqlDB, err2 := mysqlClient.DB()
	if nil != err2 {
		logrus.Errorln("mySQL设置连接池信息失败：", err2)
		return err2
	}
	// 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxIdleConns(20)
	// 设置打开数据库连接的最大数量
	sqlDB.SetMaxOpenConns(200)
	// 设置了连接可复用的最大时间
	duration, err3 := time.ParseDuration("1h")
	if nil != err3 {
		logrus.Errorln("mySQL设置连接可复用的最大时间失败：", err3)
		return err3
	}
	sqlDB.SetConnMaxLifetime(duration)
	//打印SQL配置信息
	//marshal, _ := json.Marshal(db.Stats())
	//fmt.Printf("数据库配置: %s \n", marshal)
	return nil
}

func testSqlInjection(t *testing.T) {

}

// TestSqlInjection @description: sql注入测试
// @parameter t
func TestSqlInjection(t *testing.T) {
	db.Db = mysqlClient
	workInfos := make([]WorkInfo, 0)
	workInfos = append(workInfos, WorkInfo{
		Id:       7046918689207091200,
		WorkId:   12,
		WorkName: "test",
	})
	if err := mysqlClient.Updates(&workInfos).Error; nil != err {
		logrus.Errorln("执行失败 err:", err)
	}

	var userInfos = make([]UserInfo, 0)
	ttt.YYYY()
	err := db.CommonSelect(&userInfos, ego.Field{TableName: "user_info", Where: "user_name = 'www' or 1=1"})
	//err := mysqlClient.Raw("select * from user_info where user_name = " + "'www' or 1=1").Scan(&userInfos).Error
	//var err = mysqlClient.Table("user_info").Select("*").Where("user_name = ?", "11\'1").Scan(&userInfos).Error
	if nil != err {
		logrus.Errorln("执行失败 err:", err)
	}
}
