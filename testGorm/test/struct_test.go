package test

import (
	"fmt"
	"reflect"
	"testing"
)

type Student struct {
	Age int
}

type person struct {
	Age int
}

type PersonTwo struct {
	Age  int
	Code int
}

//TestSameStructure 结构体完全相同转换
func TestSameStructure(t *testing.T) {
	var s Student = Student{10}
	var p person = person{10}
	s1 := Student(p)
	fmt.Println(s)
	fmt.Println(p)
	fmt.Println(reflect.TypeOf(s1))
}

//TestDiffStructure 部分结构体相同转换
func TestDiffStructure(t *testing.T) {
	var p PersonTwo = PersonTwo{10, 100}
	var s Student
	SourceToTarget(p, &s)
	s1 := interface{}(p).(Student)
	fmt.Println(p)
	fmt.Println(reflect.TypeOf(s1))
}

//使用反射，转换结构体
func SourceToTarget(sourceStruct interface{}, targetStruct interface{}) {
	source := structToMap(sourceStruct)
	targetV := reflect.ValueOf(targetStruct).Elem()
	targetT := reflect.TypeOf(targetStruct).Elem()
	for i := 0; i < targetV.NumField(); i++ {
		fieldName := targetT.Field(i).Name
		sourceVal := source[fieldName]
		if !sourceVal.IsValid() {
			continue

		}
		targetVal := targetV.Field(i)
		targetVal.Set(sourceVal)
	}
}

func structToMap(structName interface{}) map[string]reflect.Value {
	t := reflect.TypeOf(structName).Elem()
	v := reflect.ValueOf(structName).Elem()
	fieldNum := t.NumField()
	resMap := make(map[string]reflect.Value, fieldNum)
	for i := 0; i < fieldNum; i++ {
		resMap[t.Field(i).Name] = v.Field(i)
	}
	return resMap
}

// CopyStruct
// dst 目标结构体，src 源结构体
// 必须传入指针，且不能为nil
// 它会把src与dst的相同字段名的值，复制到dst中
func CopyStruct(dst, src interface{}) {
	dstValue := reflect.ValueOf(dst).Elem()
	srcValue := reflect.ValueOf(src).Elem()

	for i := 0; i < srcValue.NumField(); i++ {
		srcField := srcValue.Field(i)
		srcName := srcValue.Type().Field(i).Name
		dstFieldByName := dstValue.FieldByName(srcName)

		if dstFieldByName.IsValid() {
			switch dstFieldByName.Kind() {
			case reflect.Ptr:
				switch srcField.Kind() {
				case reflect.Ptr:
					if srcField.IsNil() {
						dstFieldByName.Set(reflect.New(dstFieldByName.Type().Elem()))
					} else {
						dstFieldByName.Set(srcField)
					}
				default:
					dstFieldByName.Set(srcField.Addr())
				}
			default:
				switch srcField.Kind() {
				case reflect.Ptr:
					if srcField.IsNil() {
						dstFieldByName.Set(reflect.Zero(dstFieldByName.Type()))
					} else {
						dstFieldByName.Set(srcField.Elem())
					}
				default:
					dstFieldByName.Set(srcField)
				}
			}
		}
	}
}
