package test

import "time"

// UserInfo 用户信息表 user_info
type UserInfo struct {
	Id                       int       `gorm:"AUTO_INCREMENT; primary_key; column:id"` //`id` int NOT NULL AUTO_INCREMENT  '自增ID',
	UserId                   string    `gorm:"column:user_id"`                         //`user_id` varchar(32) NOT NULL  '用户id',
	UserGroupId              string    `gorm:"column:user_group_id"`                   //`user_group_id` varchar(32) NOT NULL  '用户分组id',
	UserGroupName            string    `gorm:"column:user_group_name"`                 //`user_group_name` varchar(32)用户组名称',
	UserGroupNameGroup       string    `gorm:"column:user_group_name_group"`           //`user_group_name` varchar(32)用户组名称',
	UserName                 string    `gorm:"column:user_name"`                       //`user_name` varchar(32)用户姓名',
	Nickname                 string    `gorm:"column:nickname"`                        //`nickname` varchar(32)用户昵称',
	Role                     int       `gorm:"column:role"`                            //`role` int角色类型 0：普通用户；1：系统超级管理员；2:运维观察员；3:模型管理员',[不能更新为普通用户 4为普通用户]
	ParentNodeId             string    `gorm:"column:parent_node_id"`                  //`parent_node_id` varchar(32)父节点ID',
	Number                   string    `gorm:"column:number"`                          //`number` varchar(32)手机号',
	UserPassword             string    `gorm:"column:user_password"`                   //`user_password` varchar(32)用户密码 md5加密',
	Email                    string    `gorm:"column:emil"`                            //`emil` varchar(32)邮箱',
	Note                     string    `gorm:"column:note"`                            //`note` varchar(512)备注信息',
	CreatedTime              time.Time `gorm:"column:created_time"`                    //`created_time` datetime创建时间',
	UpdateTime               time.Time `gorm:"column:update_time"`                     //`update_time` datetime更新时间',
	LoginTime                time.Time `gorm:"column:login_time"`                      //`login_time` datetime登录时间',
	LoginIp                  string    `gorm:"column:login_ip"`                        //`login_ip` varchar(32)登录IP',
	AvatarStorageAddress     string    `gorm:"column:avatar_storage_address"`          //`avatar_storage_address` varchar(128)头像存储地址',
	IdPhotoAddress           string    `gorm:"column:Id_photo_address"`                //`Id_photo_address` varchar(128)身份证照片存储地址',
	IdNumber                 string    `gorm:"column:Id_number"`                       //`Id_number` varchar(32)身份证号码',
	IdCardVerificationStatus int       `gorm:"column:Id_card_verification_status"`     //`Id_card_verification_status` int身份证验证状态 0：待提交；1：待审核；2：审核通过；3：审核驳回，需修改信息；4：审核拒绝;',
	UserState                int       `gorm:"column:user_state"`                      //`user_state` int用户状态 1：正常；2：已禁用；',
	WeChatID                 string    `gorm:"column:WeChat_ID"`                       //`WeChat_ID` varchar(32)微信ID',
	QQId                     string    `gorm:"column:qq_id"`                           //`qq_id` varchar(32)QQ ID',
	AlternateField1          string    `gorm:"column:alternate_field1"`                //`alternate_field1` varchar(32)备用字段1',
	AlternateField2          string    `gorm:"column:alternate_field2"`                //`alternate_field2` varchar(32)备用字段2',
	AlternateField3          string    `gorm:"column:alternate_field3"`                //`alternate_field3` varchar(32)备用字段3',
}

func (UserInfo) TableName() string {
	return "user_info"
}
