package testGorm

import "time"

// User 有多张 CreditCard，UserID 是外键
type User struct {
	Id          int          `gorm:"AUTO_INCREMENT; primary_key; column:id"`
	Code        int          `gorm:"column:code"`
	Name        string       `gorm:"column:name"`
	CreditCards []CreditCard `gorm:"foreignKey:user_id"`
	//`gorm:"foreignKey:UserID;association_foreignkey:Id"`
}

// TableName 解决gorm表明映射
func (User) TableName() string {
	return "user"
}

type CreditCard struct {
	Id     int    `gorm:"AUTO_INCREMENT; primary_key; column:id"`
	UserID int    `gorm:"column:user_id"`
	Number string `gorm:"column:number"`
}

func (CreditCard) TableName() string {
	return "credit_card"
}

type SiteResource struct {
	//Id         int64  `gorm:"primary_key; column:id"`
	RelationId int64  `gorm:"column:relation_id"`
	Type       int    `gorm:"column:type"`
	Path       string `gorm:"column:path"`
	//Desc       string    `gorm:"column:desc"`
	//Name       string    `gorm:"column:name"`
	//DeleteTag  int       `gorm:"column:delete_tag"`
	//Attributes string    `gorm:"column:attributes"`
	//CreateTime time.Time `gorm:"column:create_time"`
	//ModifyTime time.Time `gorm:"column:modify_time"`
}

func (SiteResource) TableName() string {
	return "site_resource"
}

//作业信息表 work_info
type WorkInfo struct {
	Id       int64  `gorm:"primary_key; column:id;default:1" ` // ID
	WorkId   int    `gorm:"column:work_id"`                    //作业ID
	Workname string `gorm:"column:work_name"`                  //work_name VARCHAR(128)    '作业名称' ,
	DeviceId string `gorm:"column:device_id"`                  //device_id VARCHAR(32)     '设备ID' ,
	//ChannelId     int            `gorm:"column:channel_id"`      //通道ID
	Worker        string         `gorm:"column:worker"`        //worker VARCHAR(64)     '作业负责人' ,
	WorkImgUrl1   string         `gorm:"column:work_img_url1"` //work_img_url1 VARCHAR(256)     '作业准备图1' ,
	WorkImgUrl2   string         `gorm:"column:work_img_url2"` //work_img_url2 VARCHAR(256)     '作业准备图2' ,
	WorkImgUrl3   string         `gorm:"column:work_img_url3"` //work_img_url3 VARCHAR(256)     '作业准备图3' ,
	WorkVideo     string         `gorm:"column:work_video"`    //work_video VARCHAR(256)     '作业视频URL' ,
	WorkDate      time.Time      `gorm:"column:work_date"`     //work_date DATE     '作业日期' ,
	Model         string         `gorm:"column:work_model"`    //选择模式、仅录制、手机工服安全帽、动火作业、高空作业等
	StartTime     string         `gorm:"column:start_time"`    //作业开始时间
	EndTime       string         `gorm:"column:end_time"`      //作业结束时间
	DeleteTag     int            `gorm:"column:delete_tag"`    //`删除标记 0:正常；1：已删除；' ,
	CreatedTime   time.Time      `gorm:"column:created_time"`  //`创建时间' ,
	UpdateTime    time.Time      `gorm:"column:update_time"`   //`更新时间' ,
	SiteResources []SiteResource `gorm:"foreignKey:relation_id"`
}

func (WorkInfo) TableName() string {
	return "work_info"
}

//设备信息表 device_info
type DeviceInfo struct {
	Id int `gorm:"AUTO_INCREMENT; primary_key; column:id"` //`id` int NOT NULL AUTO_INCREMENT  '自增ID',

	DeviceId              string    `gorm:"column:device_id"`               //`device_id` varchar(32)设备ID',
	DeviceName            string    `gorm:"column:device_name"`             //`device_name` varchar(32)设备名称',
	DeviceGbId            string    `gorm:"column:device_gb_id"`            //`device_gb_id` varchar(32)设备国标ID',
	DeviceGroupId         string    `gorm:"column:device_group_id"`         //`device_group_id` varchar(32)设备分组ID',
	UserGroupId           string    `gorm:"column:user_group_id"`           //`user_group_id` varchar(32)用户分组id',
	DeviceGroupName       string    `gorm:"column:device_group_name"`       //`device_group_name` varchar(32)设备分组名称',
	ProductType           int       `gorm:"column:product_type"`            //`product_type` int产品类型 1:计算盒子；2:布控球；3:AI枪机 4:普通摄像头[手机] 5：服务器',6抓拍机
	ProductImgUrl         string    `gorm:"column:product_img_url"`         //`product_img_url` varchar(32)产品图片存储地址',
	HardwareModel         string    `gorm:"column:hardware_model"`          //`hardware_model` varchar(32)硬件型号',
	SoftwareName          string    `gorm:"column:software_name"`           //`software_name` varchar(32)内置软件名称',
	SoftwareVersion       string    `gorm:"column:software_version"`        //`software_version` varchar(32)内置软件版本号',
	SystemArchitecture    string    `gorm:"column:system_architecture"`     //`system_architecture` varchar(32)系统架构',
	DeviceStatus          int       `gorm:"column:device_status"`           //`device_status` int   设备状态 1:在线；2:不在线；3:未激活；4暂停服务
	DeviceInstallPosition string    `gorm:"column:device_install_position"` //`device_install_position` varchar(32)设备安装位置',
	CreatedTime           time.Time `gorm:"column:created_time"`            //`created_time` datetime创建时间',
	UpdateTime            time.Time `gorm:"column:update_time"`             //`update_time` datetime更新时间',
	DeleteTag             int       `gorm:"column:delete_tag"`              //`delete_tag` int DEFAULT '0'  '删除 0:正常；1:已删除；',
	AlternateField1       string    `gorm:"column:alternate_field1"`        //`alternate_field1` varchar(32)备用字段1',2:已激活；3:未激活；4暂停服务
	AlternateField2       string    `gorm:"column:alternate_field2"`        //`alternate_field2` varchar(32)备用字段2',支持芯片 1、agx
	DeviceType            int       `gorm:"column:device_type"`             //  `device_type` int DEFAULT NULL COMMENT '设备类型',1 信通网关 2艾瑞抓拍机
}

func (DeviceInfo) TableName() string {
	return "device_info"
}

//用户授权记录表 user_authorization
type UserAuthorization struct {
	Id int `gorm:"AUTO_INCREMENT; primary_key; column:id"` //`id` int NOT NULL AUTO_INCREMENT  '自增ID',

	DeviceId    string    `gorm:"column:device_id"`    //`device_id` varchar(32)设备ID',
	UserId      string    `gorm:"column:user_id"`      //`user_id` varchar(32)用户id',
	CreatedTime time.Time `gorm:"column:created_time"` //`created_time` datetime创建授权时间',
	DeleteTime  time.Time `gorm:"column:delete_time"`  //`delete_time` datetime删除授权时间',
	UpdateTime  time.Time `gorm:"column:update_time"`  //`update_time` datetime更新授权时间',
	DeleteTag   int       `gorm:"column:delete_tag"`   //`delete_tag` int删除标记 0:正常；1：已删除；',
}

func (UserAuthorization) TableName() string {
	return "user_authorization"
}
