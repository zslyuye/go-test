package main

import (
	"database/sql"
	"fmt"
	_ "github.com/godror/godror"
)

//func main() {
//	dsn := "airia:Airia@2314@tcp(172.10.50.238:3306)/cloudbusi?charset=utf8mb4&parseTime=True&loc=Local"
//	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
//	if err != nil {
//		log.Errorln("Error:", err)
//	}
//	workInfos := make([]testGorm.WorkInfo, 0)
//	//sql := db.ToSQL
//	db.Joins("left join device_info ON device_info.device_id = work_info.device_id").Where(map[string]interface{}{"device_info.device_id": "0506060122040078", "work_info.work_date": "2022-08-10"}).Preload("SiteResources").Find(&workInfos)
//
//	sql := db.ToSQL(func(tx *gorm.DB) *gorm.DB {
//		return db
//	})
//	fmt.Println(sql)
//	if 0 == len(workInfos) {
//		log.Infoln("查询结果为空")
//	}
//	workInfo := new(testGorm.WorkInfo)
//	//workInfo.Id = 1
//	workInfo.WorkId = 1
//	workInfo.Workname = "放假123"
//	//db.Debug().Table("work_info").Where("id = ?", 1).Update("work_name", "放假")
//	db.Debug().Where("work_name", "放假").Updates(workInfo)
//}

type Area struct {
	ID   int    `gorm:"column:ID"`
	Name string `gorm:"column:NAME"`
}

func (Area) TableName() string {
	return "T_AREA"
}

type TBBASEUSER struct {
	ID            int    `json:"ID" gorm:"column:ID"`
	APPLICATIONID int    `json:"APPLICATION_ID" gorm:"column:APPLICATION_ID"`
	USERNAME      string `json:"USER_NAME" gorm:"column:USER_NAME"`
	EMPLOYEEID    string `json:"EMPLOYEE_ID" gorm:"column:EMPLOYEE_ID"`
	IDCARD        string `json:"IDCARD" gorm:"column:IDCARD"`
}

type TBBASEORGANISEUNIT struct {
	ID               int    `json:"" gorm:"column:ID"`
	APPLICATIONID    int    `gorm:"column:APPLICATION_ID"`
	ORGANISEUNITCODE string `gorm:"column:ORGANISEUNIT_CODE"`
	STANDARDCODE     string `gorm:"column:STANDARD_CODE"`
	PARENTID         int    `gorm:"column:PARENT_ID"`
	ORGANISEUNITNAME string `gorm:"column:ORGANISEUNIT_NAME"`
	SHORTNAME        string `gorm:"column:SHORT_NAME"`
	FULLNAME         string `gorm:"column:FULL_NAME"`
	ORGANISEUNITTYPE int    `gorm:"column:ORGANISEUNIT_TYPE"`
	LOGOURL          string `gorm:"column:LOGO_URL"`
}

func main() {

	connectionString := "user/password@host:port/service_name"

	db, err := sql.Open("godror", connectionString)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// 测试查询
	rows, err := db.Query("select sysdate from dual")
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		var date string
		err = rows.Scan(&date)
		if err != nil {
			panic(err)
		}
		fmt.Println(date)
	}

}

//func main() {
//	log.Println("initial database connect……")
//	db, err := gorm.Open(oracle.Open("bpmweblink/bpmweb_my529@10.110.1.113:1521/QLORCL"), &gorm.Config{
//		Logger: logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
//			SlowThreshold: 2 * time.Millisecond,
//			LogLevel:      logger.Warn, //打印级别
//			Colorful:      true,
//		}),
//		//SkipDefaultTransaction: true,
//	})
//
//	if err != nil {
//		log.Fatalln(err)
//	}
//
//	tBBASEUSER := make([]TBBASEUSER, 0)
//	res := db.Table("TB_BASE_USER").Limit(1).Scan(&tBBASEUSER)
//	if res.Error != nil {
//		fmt.Println(res.Error)
//	}
//	fmt.Println(tBBASEUSER)
//
//	tBBASEORGANISEUNIT := make([]TBBASEORGANISEUNIT, 0)
//	res = db.Table("TB_BASE_ORGANISEUNIT").Limit(1).Scan(&tBBASEORGANISEUNIT)
//	if res.Error != nil {
//		fmt.Println(res.Error)
//	}
//
//	fmt.Println(tBBASEORGANISEUNIT)
//}
//
//var db *sql.DB
//
//func initDB() (err error) {
//	// dsn 用户名/密码@oracle机器IP:端口/服务名
//	dsn := "monitor/oracle@192.168.20.132:1521/five"
//	db, err = sql.Open("oci8", dsn)
//	if err != nil {
//		return err
//	}
//	// 尝试与数据库建立连接(校验dsn是否正确)
//	err = db.Ping()
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func main() {
//	err := initDB()
//	if err != nil {
//		fmt.Printf("init db failed,err:%s\n", err)
//		return
//	} else {
//		fmt.Println("连接成功！")
//	}
//	defer db.Close()
//	count := 0
//	err = db.QueryRow("select count(*) from TB_BASE_USER").Scan(&count)
//	if err != nil {
//		fmt.Println(err)
//		return
//	}
//	fmt.Printf("Successful  connection %v\n", count)
//	tBBASEUSER := make([]TBBASEUSER, 0)
//	err = db.QueryRow("select * from TB_BASE_USER").Scan(&tBBASEUSER)
//	if err != nil {
//		fmt.Println(err)
//		return
//	}
//	fmt.Printf("Successful  connection. Current TBBASEUSER is: %v\n", tBBASEUSER)
//}
